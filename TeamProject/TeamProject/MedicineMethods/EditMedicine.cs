﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void AddMedicine()
        {
            Globals.medicinesList = new ObservableCollection<Medicine>();
            string unit, medicineName, note, DosageUnit;
            DateTime? changeDate;
            double unitPrice, dosage;

            medicineName = tbMedicineNameMedicine.Text;
            unit = tbUnit.Text;
            note = tbNoteMedicine.Text;
            DosageUnit = tbUnitOfDosage.Text;
            if (!double.TryParse(tbUnitPrice.Text, out unitPrice))
            {
                MessageBox.Show(this, "unitPrice must be numerical", "Input error");
                return;
            }
            if (!double.TryParse(tbDosageMedicine.Text, out dosage))
            {
                MessageBox.Show(this, "dosage must be numerical", "Input error");
                return;
            }
            Medicine md = new Medicine() {MedicineName= medicineName , Dosage=dosage, Unit=unit,UnitPrice=unitPrice, DosageUnit = DosageUnit, Note = note};
            Globals.ctx = new PharmacyContext();
            Globals.ctx.MedicineCollection.Add(md);
            Globals.ctx.SaveChanges();
            MessageBox.Show("Medicine added successfully");
            //refreshMedicineList();
            refreshPatientList();
        }

    }
}
