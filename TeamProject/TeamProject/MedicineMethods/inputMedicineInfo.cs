﻿using System;
using System.IO;
using System.Windows;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void inputMedicineInfo()
        {
            string unit, medicineName, note;
            DateTime? changeDate;
            double unitPrice,dosage;

            medicineName = tbDrFirstName.Text;
            unit = tbUnit.Text;
            note = tbNoteMedicine.Text;
            if (!double.TryParse(tbUnitPrice.Text, out unitPrice))
            {
                MessageBox.Show(this, "NumberOfRefills must be numerical", "Input error");
                return;
            }
            if (!double.TryParse(tbDosageMedicine.Text, out dosage))
            {
                MessageBox.Show(this, "NumberOfRefills must be numerical", "Input error");
                return;
            }
        }
    }
}

