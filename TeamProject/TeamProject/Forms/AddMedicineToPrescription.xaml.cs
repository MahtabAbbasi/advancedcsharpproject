﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TeamProject.Classes;

namespace TeamProject.Forms
{
    /// <summary>
    /// Interaction logic for AddMedicineToPrescription.xaml
    /// </summary>
    public partial class AddMedicineToPrescription : Window
    {
        private Prescription currPer;
        private PrescriptionItem currPrescriptionItem;
        public AddMedicineToPrescription(Window parent, Prescription __currPrescription = null)
        {
            InitializeComponent();
            this.Owner = parent;
            refreshMedicineList();
            currPer = __currPrescription;
        }
        void refreshMedicineList()
        {
            try
            {
                var medicines = (from me in Globals.ctx.MedicineCollection select me).ToList();
                Globals.medicinesList = new ObservableCollection<Medicine>(medicines);
                cbMedicineList.ItemsSource = Globals.medicinesList;

            }
            catch (System.IO.InvalidDataException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void AddMedicineToPrescriptionItem_Click(object sender, RoutedEventArgs e)
        {
            Globals.PrescriptionItemList = new ObservableCollection<PrescriptionItem>();
            int quantity, refillTimes, numberOfRefills;
            string instructions;
            instructions = tbInstructions.Text;
            var selectedMedicine = cbMedicineList.SelectedItem as Medicine;
            instructions = tbInstructions.Text;
            if (!int.TryParse(tbQuantity.Text, out quantity))
            {
                MessageBox.Show(this, "Quantity must be numerical", "Input error");
                return;
            }
            if (!int.TryParse(tbRefillPeriod.Text, out refillTimes))
            {
                MessageBox.Show(this, "RefillTimes must be numerical", "Input error");
                return;
            }
            if (!int.TryParse(tbNumberOfRefills.Text, out numberOfRefills))
            {
                MessageBox.Show(this, "NumberOfRefills must be numerical", "Input error");
                return;
            }
            PrescriptionItem pmd = new PrescriptionItem()
            {
                MedicineID = selectedMedicine.MedicineID,
                MedicineName = selectedMedicine.MedicineName,
                Medicine = selectedMedicine,
                Quantity = quantity,
                NumberOfRefills = numberOfRefills,
                RefillTimes = refillTimes,
                Instructions = instructions
            };
            var preItems = (from me in Globals.ctx.PrescriptionItemCollection where me.PrescriptionID == currPer.PrescriptionID select me).ToList();
            Globals.PrescriptionItemList = new ObservableCollection<PrescriptionItem>(preItems);
            foreach (PrescriptionItem pi in Globals.PrescriptionItemList)
            {
                if (pi.MedicineID == selectedMedicine.MedicineID)
                {
                    MessageBox.Show("Each Medicine can only be added once");
                    return;
                }
            }
            Globals.ctx.PrescriptionItemCollection.Add(pmd);
            pmd.PrescriptionID = currPer.PrescriptionID;
            selectedMedicine.PrescriptionItemsCollections.Add(pmd);
            Globals.PrescriptionItemList.Add(pmd);
            Globals.ctx.SaveChanges();
            currPer.PrescriptionItemsCollections.Add(pmd);
            selectedMedicine.PrescriptionItemsCollections.Add(pmd);
            refreshPrescriptionItemListList();
            MessageBox.Show("Medicine Added");
        }
        void refreshPrescriptionItemListList()
        {
            var preItems = (from me in Globals.ctx.PrescriptionItemCollection where me.PrescriptionID == currPer.PrescriptionID select me).ToList();
            Globals.PrescriptionItemList = new ObservableCollection<PrescriptionItem>(preItems);
            lvPrescriptionItemsAddMedicine.ItemsSource = Globals.PrescriptionItemList;
        }
        private void btn_SaveMedicineListInprescription_Click(object sender, RoutedEventArgs e)
        {
            //var preItems = (from me in Globals.ctx.PrescriptionItemCollection where me.PrescriptionID == currPer.PrescriptionID select me).ToList();
            //ObservableCollection<PrescriptionItem> FullPrescriptionItemList = new ObservableCollection<PrescriptionItem>(preItems);
            this.DialogResult = true;
        }
        private void btn_DeleteAddMedicineToPrescriptionItem_Click(object sender, RoutedEventArgs e)
        {
            PrescriptionItem m = lvPrescriptionItemsAddMedicine.SelectedItem as PrescriptionItem;
            var preItems = (from me in Globals.ctx.PrescriptionItemCollection where me.PrescriptionItemID == m.PrescriptionItemID select me).ToList();
            //currPer.PrescriptionItemsCollections.Remove(preItems[0]);
            //m.Medicine.PrescriptionItemsCollections.Remove(preItems[0]);
            Globals.ctx.PrescriptionItemCollection.Remove(preItems[0]);
            Globals.PrescriptionItemList.Remove(preItems[0]);
            Globals.ctx.SaveChanges();
            MessageBox.Show("Medicine Removed successfully");
            refreshPrescriptionItemListList();
        }

        private void lvPrescriptionItems_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            PrescriptionItem selectedPrescriptionItem = lvPrescriptionItemsAddMedicine.SelectedItem as PrescriptionItem;
            if (selectedPrescriptionItem == null) return;
            tbInstructions.Text = selectedPrescriptionItem.Instructions;
            cbMedicineList.SelectedItem = selectedPrescriptionItem.Medicine;
            tbQuantity.Text = selectedPrescriptionItem.Quantity.ToString();
            tbRefillPeriod.Text = selectedPrescriptionItem.RefillTimes.ToString();
            tbNumberOfRefills.Text = selectedPrescriptionItem.NumberOfRefills.ToString();
            currPrescriptionItem = selectedPrescriptionItem;
        }
        private void btn_EditAddMedicineToPrescriptionItem_Click(object sender, RoutedEventArgs e)
        {
            int quantity, refillTimes, numberOfRefills;
            string instructions;
            instructions = tbInstructions.Text;
            var selectedMedicine = cbMedicineList.SelectedItem as Medicine;
            if (!int.TryParse(tbQuantity.Text, out quantity))
            {
                MessageBox.Show(this, "Quantity must be numerical", "Input error");
                return;
            }
            if (!int.TryParse(tbRefillPeriod.Text, out refillTimes))
            {
                MessageBox.Show(this, "RefillTimes must be numerical", "Input error");
                return;
            }
            if (!int.TryParse(tbNumberOfRefills.Text, out numberOfRefills))
            {
                MessageBox.Show(this, "NumberOfRefills must be numerical", "Input error");
                return;
            }
            currPrescriptionItem.Instructions = instructions;
            currPrescriptionItem.Medicine = selectedMedicine;
            currPrescriptionItem.Quantity = quantity;
            currPrescriptionItem.RefillTimes = refillTimes;
            currPrescriptionItem.NumberOfRefills = numberOfRefills;
            Globals.ctx.SaveChanges();
            MessageBox.Show("Medicine Updated successfully");
            refreshPrescriptionItemListList();
        }
            

    }
}
