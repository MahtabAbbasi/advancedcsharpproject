﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using TeamProject.Classes;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace TeamProject
{
    public partial class MainWindow
    {
        //public static Prescription pre;

        private void AddPrescription()
        {
            Globals.ctx = new PharmacyContext();
            string drLastName, drFirstName, drID, drEmailAddress, drPhone, drExtension, drClinicName;
            DateTime prescriptionDate;

            drLastName = tbDrLastName.Text;
            drFirstName = tbDrFirstName.Text;
            drID = tbDrId.Text;
            drEmailAddress = tbDrEmailAddress.Text;
            drPhone = tbDrPhoneNumber.Text;
            drExtension = tbDrExtention.Text;
            drClinicName = tbClinicName.Text;
            
            prescriptionDate = Convert.ToDateTime(dpPrescriptionDate.Text);
            Prescription pre = new Prescription()
            {
                Prescriptiondate=prescriptionDate,
                DrLastName=drLastName,
                DrFirstName=drFirstName,
                DrID=drID,
                DrEmailAddress=drEmailAddress,
                DrPhone=drPhone,
                DrExtension=drExtension,
                DrClinicName=drClinicName    
            };
            var selectedPatient = cmPatientList.SelectedItem as Patient;
            pre.PatientID = selectedPatient.PatientID;
            Globals.ctx.PrescriptionCollection.Add(pre);
            Globals.ctx.SaveChanges();
        }

    }
}

