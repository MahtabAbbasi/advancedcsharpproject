﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.IO;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TeamProject.Classes;
using TeamProject.Forms;
using MigraDoc;
using PdfSharp.Pdf.Content.Objects;
using MigraDoc.DocumentObjectModel.Shapes;
using ExcelLibrary.SpreadSheet;
using ExcelLibrary.CompoundDocumentFormat;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ExcelPDFDialog excelPdfDlg;

        public Prescription currPrescription;
        public Medicine currMedicine;
        public Patient currPatient;
        public Prescription currPre;
        public PrescriptionItem currPreItem;

        public MainWindow()
        {
            Globals.ctx = new PharmacyContext();
            InitializeComponent();

            Globals.excelPdfDlgAction = Enumerable.Repeat(0, 7).ToArray();

            patientsListView();

            refreshPatientList();
            reflreshAllMedicine();

            btnAddMedicine_Prescription.Visibility = Visibility.Hidden;
            btn_CalculatePrescription.Visibility = Visibility.Hidden;
            lblPayment.Visibility = Visibility.Hidden;
            tbPayment.Visibility = Visibility.Hidden;
            btn_CalculatePrescription.Visibility = Visibility.Hidden;
            btn_SavePrescription.Visibility = Visibility.Hidden;

            var pres = (from me in Globals.ctx.PrescriptionCollection select me).ToList();
            Globals.PrescriptionList = new ObservableCollection<Prescription>(pres);
            cbAllPrescriptions.ItemsSource = Globals.PrescriptionList;
        }

        private void tbFile_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
            {
                TabItem rectangle = sender as TabItem;
                ContextMenu contextMenu = rectangle.ContextMenu;
                contextMenu.PlacementTarget = rectangle;
                contextMenu.Placement = System.Windows.Controls.Primitives.PlacementMode.Bottom;
                contextMenu.IsOpen = true;
            }
        }

        private void TextBox_PreviewMedicineName(object sender, TextCompositionEventArgs e)
        {
            // Regex regex = new Regex("[^A-Z]+");
            // e.Handled = regex.IsMatch(e.Text);
        }

        private void TextBox_PreviewUnit(object sender, TextCompositionEventArgs e)
        {
            // Regex regex = new Regex(@"[0-9]+");
            // e.Handled = !regex.IsMatch(e.Text);
        }

        private void TextBox_PreviewUnitPrice(object sender, TextCompositionEventArgs e)
        {
            // Regex regex = new Regex(@"^-?[0-9]*(?:\.[0-9]*)?$");
            // e.Handled = !regex.IsMatch(e.Text);
        }

        private void TextBox_PreviewNote(object sender, TextCompositionEventArgs e)
        {
            // Regex regex = new Regex("[^A-Z]+");
            // e.Handled = regex.IsMatch(e.Text);
        }

        private void tbMedicine_GotFocus(object sender, EventArgs e)
        {
            // tbMedicineNameMedicine.Clear();
        }

        private void tbUnit_GotFocus(object sender, EventArgs e)
        {
            // tbUnit.Clear();
        }

        private void tbUnitPrice_GotFocus(object sender, EventArgs e)
        {
            // tbUnitPrice.Clear();
        }

        private void tbNote_GotFocus(object sender, EventArgs e)
        {
            // tbNoteMedicine.Clear();
        }

        private void tbMedicine_LostFocus(object sender, RoutedEventArgs e)
        {
            // string mName = tbMedicineNameMedicine.Text;
            // bool isError = mName.Length < 2 || mName.Length > 50;
            //lblMedicineNameError.Visibility = isError ? Visibility.Visible : Visibility.Hidden;
        }

        private void tbUnit_LostFocus(object sender, RoutedEventArgs e)
        {
            // int unit = 0;
            // bool isError = !int.TryParse(tbUnit.Text, out unit);
            // lblUnitError.Visibility = isError ? Visibility.Visible : Visibility.Hidden;
        }

        private void tbUnitPrice_LostFocus(object sender, RoutedEventArgs e)
        {
            // double unitPrice = 0.0d;
            // bool isError = !double.TryParse(tbUnitPrice.Text, out unitPrice);
            // lblUnitPriceError.Visibility = isError ? Visibility.Visible : Visibility.Hidden;
        }

        private void tbNote_LostFocus(object sender, RoutedEventArgs e)
        {
            // string note = tbNoteMedicine.Text;
            // bool isError = note.Length > 150;
            // lblNoteError.Visibility = isError ? Visibility.Visible : Visibility.Hidden;
        }

        public void btnAddPatientForm_Click(object sender, RoutedEventArgs e)
        {
            AddPatient();
            refreshPatientList();
        }

        public void btnDeletePatientForm_Click(object sender, RoutedEventArgs e)
        {
            Patient selectedPatient = lvPatientAll.SelectedItem as Patient;
            Globals.ctx.PatientCollection.Remove(selectedPatient);
            Globals.ctx.SaveChanges();
            MessageBox.Show("Patient Deleted successfully");
            refreshPatientList();
        }

        public void btnUpdatePatientForm_Click(object sender, RoutedEventArgs e)
        {
            Patient.GenderEnum gender;
            currPatient.FirstName = tbPPFirstName.Text;
            currPatient.LastName = tbPPLastName.Text;
            currPatient.BirthDate = Convert.ToDateTime(dpPPBirthDate.Text);
            if (!Enum.TryParse<Patient.GenderEnum>(cbxPPGender.Text, result: out gender))
                throw new InvalidDataException("The format of gender enum is not correct." + cbxPPGender.Text);
            currPatient.Gender = gender;
            currPatient.Address = tbPCAddress.Text;
            currPatient.City = tbPCCity.Text;
            currPatient.Region = tbPCRegion.Text;
            currPatient.PostalCode = tbPCPostalCode.Text;
            currPatient.Country = tbPCCountry.Text;
            currPatient.CellPhone = tbPCCellPhone.Text;
            currPatient.HomePhone = tbPCHomePhone.Text;
            currPatient.FamilyDrID = tbFDIDrId.Text;
            currPatient.FamilyDrFirstName = tbFDIFirstName.Text;
            currPatient.FamilyDrLastName = tbFDILastName.Text;
            currPatient.FamilyDrClinicName = tbFDIClinicName.Text;
            currPatient.FamilyDrPhone = tbFDIPhoneNumber.Text;
            currPatient.FamilyDrExtension = tbFDIExt.Text;
            currPatient.FamilyDrExtension = tbFDIEmailAdd.Text;

            currPatient.Alergy = "";
            if (chbMHAntibiotics.IsChecked == true) currPatient.Alergy += "Antibiotics,";
            if (chbMHSulfa.IsChecked == true) currPatient.Alergy += "Sulfa,";
            if (chbMHAspirin.IsChecked == true) currPatient.Alergy += "Aspirin,";
            if (chbMHInsulin.IsChecked == true) currPatient.Alergy += "Insulin,";
            if (currPatient.Alergy.EndsWith(@",")) currPatient.Alergy = currPatient.Alergy.Substring(0, currPatient.Alergy.Length - 1);

            currPatient.note = tbMHNotes.Text;
            string strCardID = tbPICardId.Text;
            int cardID;
            double coveragePercent;
            if (!int.TryParse(strCardID, out cardID))
                throw new InvalidDataException("The format of insurance card id is not correct.");
            currPatient.InsuranceCardID = cardID;
            string strcoveragePercent = tbPICoverage.Text;
            if (!Double.TryParse(strcoveragePercent, out coveragePercent))
                throw new InvalidDataException("The format of insurance coverage is not correct.");
            currPatient.Insurance.CoveragePercent = coveragePercent;
            currPatient.Insurance.InsuranceCompanyName = tbPICampanyName.Text;
            currPatient.Insurance.InsuranceType = tbPIInsuranceType.Text;
            Globals.ctx.SaveChanges();
            MessageBox.Show("Patient Updated successfully");
            refreshPatientList();
        }

        void refreshPatientList()
        {
            try
            {
                var Patients = (from me in Globals.ctx.PatientCollection select me).ToList();
                Globals.patientsList = new ObservableCollection<Patient>(Patients);
                cmPatientList.ItemsSource = Globals.patientsList;
                lvPatientAll.ItemsSource = Globals.patientsList;

            }
            catch (System.IO.InvalidDataException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lvPatientAll_Selectionchanged(object sender, SelectionChangedEventArgs e)
        {
            Patient selectedPatient = lvPatientAll.SelectedItem as Patient;
            if (selectedPatient == null) return;
            lblPPId.Content = selectedPatient.PatientID.ToString();
            tbPPFirstName.Text = selectedPatient.FirstName;
            tbPPLastName.Text = selectedPatient.LastName;
            dpPPBirthDate.SelectedDate = selectedPatient.BirthDate;
            cbxPPGender.Text = selectedPatient.Gender.ToString();
            tbPCAddress.Text = selectedPatient.Address;
            tbPCCity.Text = selectedPatient.City;
            tbPCRegion.Text = selectedPatient.Region;
            tbPCPostalCode.Text = selectedPatient.PostalCode;
            tbPCCountry.Text = selectedPatient.Country;
            tbPCCellPhone.Text = selectedPatient.CellPhone;
            tbPCHomePhone.Text = selectedPatient.HomePhone;
            tbFDIDrId.Text = selectedPatient.FamilyDrID;
            tbFDIFirstName.Text = selectedPatient.FamilyDrFirstName;
            tbFDILastName.Text = selectedPatient.FamilyDrLastName;
            tbFDIClinicName.Text = selectedPatient.FamilyDrClinicName;
            tbFDIPhoneNumber.Text = selectedPatient.FamilyDrPhone;
            tbFDIExt.Text = selectedPatient.FamilyDrExtension;
            tbFDIEmailAdd.Text = selectedPatient.FamilyDrExtension;

            var alergy = selectedPatient.Alergy.Split(',');
            foreach (var a in alergy)
            {
                if (a.Equals("Antibiotics")) chbMHAntibiotics.IsChecked = true;
                if (a.Equals("Sulfa")) chbMHSulfa.IsChecked = true;
                if (a.Equals("Aspirin")) chbMHAspirin.IsChecked = true;
                if (a.Equals("Insulin")) chbMHInsulin.IsChecked = true;
            }

            tbMHNotes.Text = selectedPatient.note;
            tbPICardId.Text = selectedPatient.InsuranceCardID.ToString();
            tbPICoverage.Text = selectedPatient.Insurance.CoveragePercent.ToString();
            tbPICampanyName.Text = selectedPatient.Insurance.InsuranceCompanyName;
            tbPIInsuranceType.Text = selectedPatient.Insurance.InsuranceType;
            currPatient = selectedPatient;
        }

        public void btnAddMedicinForm_Click(object sender, RoutedEventArgs e)
        {
            AddMedicine();
            reflreshAllMedicine();
        }

        public void btnEditMedicineForm_Click(object sender, RoutedEventArgs e)
        {
            string unit, medicineName, note, DosageUnit;
            double unitPrice, dosage;
            medicineName = tbMedicineNameMedicine.Text;
            unit = tbUnit.Text;
            note = tbNoteMedicine.Text;
            DosageUnit = tbUnitOfDosage.Text;
            if (!double.TryParse(tbUnitPrice.Text, out unitPrice))
            {
                MessageBox.Show(this, "unitPrice must be numerical", "Input error");
                return;
            }
            if (!double.TryParse(tbDosageMedicine.Text, out dosage))
            {
                MessageBox.Show(this, "dosage must be numerical", "Input error");
                return;
            }
            currMedicine.MedicineName = medicineName;
            currMedicine.Unit = unit;
            currMedicine.Note = note;
            currMedicine.DosageUnit = DosageUnit;
            currMedicine.Dosage = dosage;
            currMedicine.UnitPrice = unitPrice;
            Globals.ctx.SaveChanges();
            MessageBox.Show("Medicine Updated successfully");
            reflreshAllMedicine();
        }

        public void btnDeleteMedicineForm_Click(object sender, RoutedEventArgs e)
        {
            Medicine selectedMedicine = lvMedicinesAll.SelectedItem as Medicine;
            Globals.ctx.MedicineCollection.Remove(selectedMedicine);
            Globals.ctx.SaveChanges();
            MessageBox.Show("Medicine Deleted successfully");
            reflreshAllMedicine();
        }

        void reflreshAllMedicine()
        {
            try
            {
                var medicines = (from me in Globals.ctx.MedicineCollection select me).ToList();
                Globals.medicinesList = new ObservableCollection<Medicine>(medicines);
                lvMedicinesAll.ItemsSource = Globals.medicinesList;
            }
            catch (System.IO.InvalidDataException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void lvMedicinesAll_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Medicine selectedMedicine = lvMedicinesAll.SelectedItem as Medicine;
            if (selectedMedicine == null) return;
            lblMedicineIdContent.Content = selectedMedicine.ToString();
            tbMedicineNameMedicine.Text = selectedMedicine.MedicineName;
            tbUnit.Text = selectedMedicine.Unit.ToString();
            tbNoteMedicine.Text = selectedMedicine.Note;
            tbUnitOfDosage.Text = selectedMedicine.DosageUnit;
            tbDosageMedicine.Text = selectedMedicine.Dosage.ToString();
            tbUnitPrice.Text = selectedMedicine.UnitPrice.ToString();
            currMedicine = selectedMedicine;
        }

        private void btnAddPrescription_Click(object sender, RoutedEventArgs e)
        {
            AddPrescription();

            string drLastName, drFirstName, drID, drEmailAddress, drPhone, drExtension, drClinicName;
            DateTime prescriptionDate;

            drLastName = tbDrLastName.Text;
            drFirstName = tbDrFirstName.Text;
            drID = tbDrId.Text;
            drEmailAddress = tbDrEmailAddress.Text;
            drPhone = tbDrPhoneNumber.Text;
            drExtension = tbDrExtention.Text;
            drClinicName = tbClinicName.Text;
            prescriptionDate = Convert.ToDateTime(dpPrescriptionDate.Text);
            Prescription pre = new Prescription()
            {
                Prescriptiondate = prescriptionDate,
                DrLastName = drLastName,
                DrFirstName = drFirstName,
                DrID = drID,
                DrEmailAddress = drEmailAddress,
                DrPhone = drPhone,
                DrExtension = drExtension,
                DrClinicName = drClinicName
            };
            var selectedPatient = cmPatientList.SelectedItem as Patient;
            pre.PatientID = selectedPatient.PatientID;
            Globals.ctx.PrescriptionCollection.Add(pre);
            currPrescription = pre;
            Globals.ctx.SaveChanges();
            selectedPatient.PrescriptionsCollection.Add(pre);
            MessageBox.Show("Prescription Created successfully");

            btnAddMedicine_Prescription.Visibility = Visibility.Visible;
        }

        private void btn_SavePrescription_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Prescription Added successfully");
            tbDrLastName.Text = "";
            tbDrFirstName.Text = "";
            tbDrId.Text = "";
            tbDrEmailAddress.Text = "";
            tbDrPhoneNumber.Text = "";
            tbDrExtention.Text = "";
            tbClinicName.Text = "";
            tbPayment.Text = "";
            dpPrescriptionDate.Text = DateTime.Now.ToString();
            cmPatientList.SelectedIndex = -1;
            //lvPrescriptionItems.Items.Clear();
        }

        private void btnAddMedicine_Click(object sender, RoutedEventArgs e)
        {
            AddMedicineToPrescription dlg = new AddMedicineToPrescription(this, currPrescription);
            if (dlg.ShowDialog() == true)
            {
                var preItems = (from me in Globals.ctx.PrescriptionItemCollection where me.PrescriptionID == currPrescription.PrescriptionID select me).ToList();
                Globals.PrescriptionItemList = new ObservableCollection<PrescriptionItem>(preItems);
                lvPrescriptionItems.ItemsSource = Globals.PrescriptionItemList;
                MessageBox.Show("Success!");
            }

            btn_CalculatePrescription.Visibility = Visibility.Visible;
        }

        private void btn_CalculatePrescription_Click(object sender, RoutedEventArgs e)
        {
            var preItems = (from me in Globals.ctx.PrescriptionItemCollection where me.PrescriptionID == currPrescription.PrescriptionID select me).ToList();
            Globals.PrescriptionItemList = new ObservableCollection<PrescriptionItem>(preItems);
            foreach (PrescriptionItem pi in Globals.PrescriptionItemList)
            {
                Refill refill = new Refill()
                {
                    RefillDate = pi.Prescription.Prescriptiondate,
                    PrescriptionItemID = pi.PrescriptionItemID,
                    PrescriptionItem = pi,
                    note = ""
                };
                Globals.ctx.RefillCollection.Add(refill);
                pi.RefikksCollection.Add(refill);
                Payment payment = new Payment()
                {
                    PaymentAmount = pi.Medicine.UnitPrice * pi.Quantity,
                    PaymentDate = pi.Prescription.Prescriptiondate,
                    PaymentType = "",
                    RefillId = refill.RefillID
                };
                Globals.ctx.PaymentCollection.Add(payment);
                Globals.ctx.SaveChanges();
                refill.PaymentCollection.Add(payment);

            }
            double total = 0;
            foreach (PrescriptionItem pi in Globals.PrescriptionItemList)
            {
                var preRefills = (from p in pi.RefikksCollection select p).ToList();
                Refill cr = preRefills[0];
                var prePayments = (from p in cr.PaymentCollection select p).ToList();
                total += prePayments[0].PaymentAmount;
            }
            total += (total * 15) / 100;
            tbPayment.Text = total.ToString();
            currPrescription.TotalPayment = total;

            lblPayment.Visibility = Visibility.Visible;
            tbPayment.Visibility = Visibility.Visible;
            btn_SavePrescription.Visibility = Visibility.Visible;
        }

        void refreshPrescriptionItemListList()
        {
            var preItems = (from me in Globals.ctx.PrescriptionItemCollection where me.PrescriptionID == currPrescription.PrescriptionID select me).ToList();
            Globals.PrescriptionItemList = new ObservableCollection<PrescriptionItem>(preItems);
            lvPrescriptionItems.ItemsSource = Globals.PrescriptionItemList;
        }

        private void cbAllPrescriptions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Prescription p = cbAllPrescriptions.SelectedItem as Prescription;
            var preItems = (from me in Globals.ctx.PrescriptionItemCollection where me.PrescriptionID == p.PrescriptionID select me).ToList();
            Globals.PrescriptionItemList = new ObservableCollection<PrescriptionItem>(preItems);
            cbRefilItem.ItemsSource = Globals.PrescriptionItemList;
            currPre = p;
        }

        private void btn_AddRefillItem_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Refill added Successfully");
            //cbAllPrescriptions.SelectedIndex = -1;
            //cbRefilItem.SelectedIndex = -1;
            dpRefillItem.SelectedDate = null;
            tbRefillNote.Text = "";
        }

        private void btn_calculatePayment_Click(object sender, RoutedEventArgs e)
        {
            PrescriptionItem pi = cbRefilItem.SelectedItem as PrescriptionItem;
            currPreItem = pi;
            if (pi.NumberOfRefills >= pi.RefillsDone)
            {
                Refill refill = new Refill()
                {
                    RefillDate = Convert.ToDateTime(dpRefillItem.Text),
                    PrescriptionItemID = pi.PrescriptionItemID,
                    PrescriptionItem = pi,
                    note = tbRefillNote.Text
                };
                Globals.ctx.RefillCollection.Add(refill);
                pi.RefikksCollection.Add(refill);
                Payment payment = new Payment()
                {
                    PaymentAmount = pi.Medicine.UnitPrice * pi.Quantity,
                    PaymentDate = pi.Prescription.Prescriptiondate,
                    PaymentType = "",
                    RefillId = refill.RefillID
                };
                Globals.ctx.PaymentCollection.Add(payment);
                Globals.ctx.SaveChanges();
                pi.RefillsDone++;
                refill.PaymentCollection.Add(payment);
                double total = payment.PaymentAmount;
                total += (total * 15) / 100;
                pi.Prescription.TotalPayment = total;
                tbPaymenRefill.Text = total.ToString();
            }
            else
            {
                MessageBox.Show("No refill is available for this item");
            }
        }

        private void MenuItemPdf_Click(object sender, RoutedEventArgs e)
        {
            excelPdfDlg = new ExcelPDFDialog(this);
            excelPdfDlg.lblSelect.Content = "Select tables to be involve in the pdf file:";
            excelPdfDlg.lblExcelPdfFileName.Content = "Pdf File Name:";
            excelPdfDlg.btnCreateExcelPdf.Content = "Create Pdf File";

            if (excelPdfDlg.ShowDialog() == true)
            {
                //First lists are filled up.
                activeLists();

                exportToPdf();
                MessageBox.Show("Pdf File was created successfully.");
            }
        }

        private void MenuItemExcel_Click(object sender, RoutedEventArgs e)
        {
            excelPdfDlg = new ExcelPDFDialog(this);
            excelPdfDlg.lblSelect.Content = "Select tables to be involve in the Excel file:";
            excelPdfDlg.lblExcelPdfFileName.Content = "Excel File Name:";
            excelPdfDlg.btnCreateExcelPdf.Content = "Create Excel File";

            if (excelPdfDlg.ShowDialog() == true)
            {
                //First lists are filled up.
                activeLists();

                exportToExcel();
                MessageBox.Show("Excel File was created successfully.");
            }
            return;
        }

        private void MenuItemPrint_Click(object sender, RoutedEventArgs e)
        {
            excelPdfDlg = new ExcelPDFDialog(this);
            excelPdfDlg.lblSelect.Content = "Select tables to be involved in printing:";
            excelPdfDlg.lblExcelPdfFileName.Visibility = Visibility.Hidden;
            excelPdfDlg.tbExcelPdfFileName.Visibility = Visibility.Hidden;
            excelPdfDlg.btnCreateExcelPdf.Content = "Print";

            if (excelPdfDlg.ShowDialog() == true)
            {
                //First lists are filled up.
                activeLists();

                printDocument();
                MessageBox.Show("Selected items(s) was printed successfully.");
            }
        }

        private void cmbReportField_DropDownClosed(object sender, EventArgs e)
        {
            string str = cmbReportField.Text;
            switch (str)
            {
                case "Prescription":
                    prescriptionsListView();
                    break;

                case "Prescription Items":
                    prescriptionItemsListView();
                    break;

                case "Refill":
                    refillsListView();
                    break;

                case "Medicine":
                    medicinesListView();
                    break;

                case "Payment":
                    paymentsListView();
                    break;

                case "Insurance":
                    insuranceListView();
                    break;

                default:
                    patientsListView();
                    break;
            }
            return;
        }

        private void btnPdfReport_Click(object sender, RoutedEventArgs e)
        {
            Globals.excelPdfFileName = tbReportFileName.Text;

            for (int i = 0; i < 7; ++i)
                Globals.excelPdfDlgAction[i] = 0;

            string str = cmbReportField.Text;
            switch (str)
            {
                case "Prescription":
                    Globals.excelPdfDlgAction[1] = 1;
                    break;

                case "Prescription Items":
                    Globals.excelPdfDlgAction[2] = 1;
                    break;

                case "Refill":
                    Globals.excelPdfDlgAction[3] = 1;
                    break;

                case "Medicine":
                    Globals.excelPdfDlgAction[4] = 1;
                    break;

                case "Payment":
                    Globals.excelPdfDlgAction[5] = 1;
                    break;

                case "Insurance":
                    Globals.excelPdfDlgAction[6] = 1;
                    break;

                default:
                    Globals.excelPdfDlgAction[0] = 1;
                    break;
            }
            exportToPdf();
            MessageBox.Show("Pdf File was created successfully.");
            tbReportFileName.Text = "Only File Name (at least 2 character)";
            return;
        }

        private void btnExcelReport_Click(object sender, RoutedEventArgs e)
        {
            Globals.excelPdfFileName = tbReportFileName.Text;

            for (int i = 0; i < 7; ++i)
                Globals.excelPdfDlgAction[i] = 0;

            string str = cmbReportField.Text;
            switch (str)
            {
                case "Prescription":
                    Globals.excelPdfDlgAction[1] = 1;
                    break;

                case "Prescription Items":
                    Globals.excelPdfDlgAction[2] = 1;
                    break;

                case "Refill":
                    Globals.excelPdfDlgAction[3] = 1;
                    break;

                case "Medicine":
                    Globals.excelPdfDlgAction[4] = 1;
                    break;

                case "Payment":
                    Globals.excelPdfDlgAction[5] = 1;
                    break;

                case "Insurance":
                    Globals.excelPdfDlgAction[6] = 1;
                    break;

                default:
                    Globals.excelPdfDlgAction[0] = 1;
                    break;
            }
            exportToExcel();
            MessageBox.Show("Excel File was created successfully.");
            tbReportFileName.Text = "Only File Name (at least 2 character)";
            return;
        }

        private void btnPrintReport_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 7; ++i)
                Globals.excelPdfDlgAction[i] = 0;

            string str = cmbReportField.Text;
            switch (str)
            {
                case "Prescription":
                    Globals.excelPdfDlgAction[1] = 1;
                    break;

                case "Prescription Items":
                    Globals.excelPdfDlgAction[2] = 1;
                    break;

                case "Refill":
                    Globals.excelPdfDlgAction[3] = 1;
                    break;

                case "Medicine":
                    Globals.excelPdfDlgAction[4] = 1;
                    break;

                case "Payment":
                    Globals.excelPdfDlgAction[5] = 1;
                    break;

                case "Insurance":
                    Globals.excelPdfDlgAction[6] = 1;
                    break;

                default:
                    Globals.excelPdfDlgAction[0] = 1;
                    break;
            }
            printDocument();
            MessageBox.Show("This table was printed successfully.");
            return;
        }

        private void cmbSearchDatabase_DropDownClosed(object sender, EventArgs e)
        {
            if (!(cmbSearchElement.Items is null))
                cmbSearchElement.Items.Clear();

            string str = cmbSearchDatabase.Text;
            switch (str)
            {
                case "Prescription":
                    cmbSearchElement.Items.Add("Prescription Id");
                    cmbSearchElement.Items.Add("Patient Id");
                    cmbSearchElement.Items.Add("Dr's Name");
                    cmbSearchElement.Items.Add("Dr's Id");
                    cmbSearchElement.Items.Add("Email");
                    cmbSearchElement.Items.Add("Phone No.");
                    cmbSearchElement.Items.Add("Extension");
                    cmbSearchElement.Items.Add("Clinic Name");
                    cmbSearchElement.Items.Add("Note");
                    break;

                case "Prescription Items":
                    cmbSearchElement.Items.Add("Prescription Item Id");
                    cmbSearchElement.Items.Add("Prescription Id");
                    cmbSearchElement.Items.Add("Medicine Id");
                    cmbSearchElement.Items.Add("Number Of Refills");
                    cmbSearchElement.Items.Add("Refills Done");
                    cmbSearchElement.Items.Add("Refill Times");
                    cmbSearchElement.Items.Add("Instructions");
                    break;

                case "Refill":
                    cmbSearchElement.Items.Add("Refill Id");
                    cmbSearchElement.Items.Add("Prescription Item Id");
                    cmbSearchElement.Items.Add("Note");
                    break;

                case "Medicine":
                    cmbSearchElement.Items.Add("Medicine Id");
                    cmbSearchElement.Items.Add("Medicine Name");
                    cmbSearchElement.Items.Add("Dosage");
                    cmbSearchElement.Items.Add("Unit of Dosage");
                    cmbSearchElement.Items.Add("Unit");
                    cmbSearchElement.Items.Add("Unit Price");
                    cmbSearchElement.Items.Add("Note");
                    break;

                case "Payment":
                    cmbSearchElement.Items.Add("Payment Id");
                    cmbSearchElement.Items.Add("Payment Amount");
                    cmbSearchElement.Items.Add("Payment Type");
                    cmbSearchElement.Items.Add("Refill Id");
                    break;

                case "Insurance":
                    cmbSearchElement.Items.Add("Insurance Card Id");
                    cmbSearchElement.Items.Add("Coverage Percent");
                    cmbSearchElement.Items.Add("Insurance Company Name");
                    cmbSearchElement.Items.Add("Insurance Type");
                    break;

                default:
                    cmbSearchElement.Items.Add("Patient Id");
                    cmbSearchElement.Items.Add("First Name");
                    cmbSearchElement.Items.Add("Last Name");
                    cmbSearchElement.Items.Add("Gender");
                    cmbSearchElement.Items.Add("Insurance Card Id");
                    cmbSearchElement.Items.Add("Adress");
                    cmbSearchElement.Items.Add("City");
                    cmbSearchElement.Items.Add("Region");
                    cmbSearchElement.Items.Add("Postal Code");
                    cmbSearchElement.Items.Add("Country");
                    cmbSearchElement.Items.Add("Home Phone No.");
                    cmbSearchElement.Items.Add("Cell Phone No.");
                    cmbSearchElement.Items.Add("Dr's First Name");
                    cmbSearchElement.Items.Add("Dr's Last Name");
                    cmbSearchElement.Items.Add("Dr's Id");
                    cmbSearchElement.Items.Add("Email");
                    cmbSearchElement.Items.Add("Phone No.");
                    cmbSearchElement.Items.Add("Extension");
                    cmbSearchElement.Items.Add("Clinic Name");
                    cmbSearchElement.Items.Add("Note");
                    break;
            }
            cmbSearchElement.SelectedItem = cmbSearchElement.Items[0];
            return;
        }

        private void btnSearch_Click(object sender, RoutedEventArgs e)
        {
            string Strkeywords;
            string[] splitString;
            if (cmbSearchDatabase.Text.Equals("Patient"))
            {
                if (cmbSearchElement.Text.Equals("Patient Id"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.PatientID.ToString().Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("First Name"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.FirstName.Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Last Name"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.LastName.Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Gender"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.Gender.ToString().Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Insurance Card ID"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.InsuranceCardID.ToString().Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Address"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.Address.Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("City"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.City.Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Region"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.Region.Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Postal Code"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.PostalCode.Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Home Phone No."))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.HomePhone.Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Cell Phone No."))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.CellPhone.Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Dr's Name"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.FamilyDrFirstName.Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Dr's Id"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.FamilyDrID.ToString().Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Email"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.FamilyDrEmailAddress.Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Phone No."))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.FamilyDrPhone.Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Extension"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.FamilyDrExtension.Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Clinic Name"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.FamilyDrClinicName.Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Note"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var patKeywords = splitString.ToList();
                    var pat = Globals.ctx.PatientCollection.Where(q => patKeywords.Any(k => q.note.Contains(k)));
                    Globals.patientsList = new ObservableCollection<Patient>(pat);
                    SelectedPatientsListView();
                    MessageBox.Show("Search is completed.");
                }

                var patients = (from me in Globals.ctx.PatientCollection select me).ToList();
                Globals.patientsList = new ObservableCollection<Patient>(patients);
            }
            else if (cmbSearchDatabase.Text.Equals("Prescription"))
            {
                if (cmbSearchElement.Text.Equals("Prescription Id"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var preKeywords = splitString.ToList();
                    var pre = Globals.ctx.PrescriptionCollection.Where(q => preKeywords.Any(k => q.PrescriptionID.ToString().Contains(k)));
                    Globals.PrescriptionList = new ObservableCollection<Prescription>(pre);
                    SelectedPrescriptionsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Patient Id"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var preKeywords = splitString.ToList();
                    var pre = Globals.ctx.PrescriptionCollection.Where(q => preKeywords.Any(k => q.PatientID.ToString().Contains(k)));
                    Globals.PrescriptionList = new ObservableCollection<Prescription>(pre);
                    SelectedPrescriptionsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Dr's Name"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var preKeywords = splitString.ToList();
                    var pre = Globals.ctx.PrescriptionCollection.Where(q => preKeywords.Any(k => q.DrFirstName.Contains(k)));
                    Globals.PrescriptionList = new ObservableCollection<Prescription>(pre);
                    SelectedPrescriptionsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Dr's Name"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var preKeywords = splitString.ToList();
                    var pre = Globals.ctx.PrescriptionCollection.Where(q => preKeywords.Any(k => q.DrLastName.Contains(k)));
                    Globals.PrescriptionList = new ObservableCollection<Prescription>(pre);
                    SelectedPrescriptionsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Dr's Id"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var preKeywords = splitString.ToList();
                    var pre = Globals.ctx.PrescriptionCollection.Where(q => preKeywords.Any(k => q.DrID.ToString().Contains(k)));
                    Globals.PrescriptionList = new ObservableCollection<Prescription>(pre);
                    SelectedPrescriptionsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Emaile"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var preKeywords = splitString.ToList();
                    var pre = Globals.ctx.PrescriptionCollection.Where(q => preKeywords.Any(k => q.DrEmailAddress.Contains(k)));
                    Globals.PrescriptionList = new ObservableCollection<Prescription>(pre);
                    SelectedPrescriptionsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Phone No."))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var preKeywords = splitString.ToList();
                    var pre = Globals.ctx.PrescriptionCollection.Where(q => preKeywords.Any(k => q.DrPhone.Contains(k)));
                    Globals.PrescriptionList = new ObservableCollection<Prescription>(pre);
                    SelectedPrescriptionsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Extension"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var preKeywords = splitString.ToList();
                    var pre = Globals.ctx.PrescriptionCollection.Where(q => preKeywords.Any(k => q.DrExtension.Contains(k)));
                    Globals.PrescriptionList = new ObservableCollection<Prescription>(pre);
                    SelectedPrescriptionsListView();
                    MessageBox.Show("Search is completed.");
                }
                else if (cmbSearchElement.Text.Equals("Clinic Name"))
                {
                    Strkeywords = tbSearchKeyword.Text;
                    splitString = Strkeywords.Split();
                    var preKeywords = splitString.ToList();
                    var pre = Globals.ctx.PrescriptionCollection.Where(q => preKeywords.Any(k => q.DrClinicName.Contains(k)));
                    Globals.PrescriptionList = new ObservableCollection<Prescription>(pre);
                    SelectedPrescriptionsListView();
                    MessageBox.Show("Search is completed.");
                }

                var prescriptions = (from me in Globals.ctx.PrescriptionCollection select me).ToList();
                Globals.PrescriptionList = new ObservableCollection<Prescription>(prescriptions);
            }
            else if (cmbSearchDatabase.Text.Equals("Prescription Items"))
            {
                Strkeywords = tbSearchKeyword.Text;
                splitString = Strkeywords.Split();
                var patKeywords = splitString.ToList();
                var pat = Globals.ctx.PrescriptionItemCollection.Where(q => patKeywords.Any(k => q.PrescriptionID.ToString().Contains(k)));
                Globals.PrescriptionItemList = new ObservableCollection<PrescriptionItem>(pat);
                SelectedPrescriptionItemsListView();
                MessageBox.Show("Search is completed.");

                var prescriptionItems = (from me in Globals.ctx.PrescriptionItemCollection select me).ToList();
                Globals.PrescriptionItemList = new ObservableCollection<PrescriptionItem>(prescriptionItems);
            }

            else if (cmbSearchDatabase.Text.Equals("Refill"))
            {
                Strkeywords = tbSearchKeyword.Text;
                splitString = Strkeywords.Split();
                var patKeywords = splitString.ToList();
                var pat = Globals.ctx.RefillCollection.Where(q => patKeywords.Any(k => q.RefillID.ToString().Contains(k)));
                Globals.RefillList = new ObservableCollection<Refill>(pat);
                SelectedRefillsListView();
                MessageBox.Show("Search is completed.");

                var refills = (from me in Globals.ctx.RefillCollection select me).ToList();
                Globals.RefillList = new ObservableCollection<Refill>(refills);
            }

            else if (cmbSearchDatabase.Text.Equals("Medicine"))
            {
                Strkeywords = tbSearchKeyword.Text;
                splitString = Strkeywords.Split();
                var patKeywords = splitString.ToList();
                var pat = Globals.ctx.MedicineCollection.Where(q => patKeywords.Any(k => q.MedicineName.Contains(k)));
                Globals.medicinesList = new ObservableCollection<Medicine>(pat);
                SelectedMedicinesListView();
                MessageBox.Show("Search is completed.");

                var medicines = (from me in Globals.ctx.MedicineCollection select me).ToList();
                Globals.medicinesList = new ObservableCollection<Medicine>(medicines);
            }

            else if (cmbSearchDatabase.Text.Equals("Payment"))
            {
                Strkeywords = tbSearchKeyword.Text;
                splitString = Strkeywords.Split();
                var patKeywords = splitString.ToList();
                var pat = Globals.ctx.PaymentCollection.Where(q => patKeywords.Any(k => q.PaymentID.ToString().Contains(k)));
                Globals.PaymentsList = new ObservableCollection<Payment>(pat);
                SelectedPaymentsListView();
                MessageBox.Show("Search is completed.");

                var Payments = (from me in Globals.ctx.PaymentCollection select me).ToList();
                Globals.PaymentsList = new ObservableCollection<Payment>(Payments);
            }

            else if (cmbSearchDatabase.Text.Equals("Insurance"))
            {
                Strkeywords = tbSearchKeyword.Text;
                splitString = Strkeywords.Split();
                var patKeywords = splitString.ToList();
                var pat = Globals.ctx.InsuranceCollection.Where(q => patKeywords.Any(k => q.InsuranceCompanyName.Contains(k)));
                Globals.insuranceList = new ObservableCollection<Insurance>(pat);
                SelectedInsuranceListView();
                MessageBox.Show("Search is completed.");

                var insurance = (from me in Globals.ctx.InsuranceCollection select me).ToList();
                Globals.insuranceList = new ObservableCollection<Insurance>(insurance);
            }
            return;
        }

        private void tbSearchKeyword_GotFocus(object sender, RoutedEventArgs e)
        {
            tbSearchKeyword.Clear();
        }

        private void tbReportFileName_GotFocus(object sender, RoutedEventArgs e)
        {
            tbReportFileName.Clear();
        }
    }
}
