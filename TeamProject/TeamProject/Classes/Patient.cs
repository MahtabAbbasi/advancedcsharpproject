﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamProject
{
    public class Patient
    {
        [Key]
        public int PatientID { get; set; }

        [Required]
        public string LastName { get; set; }

        public enum GenderEnum { NA = 0, Female = 1, Male = 2 };

        [Required]
        public GenderEnum Gender { get; set; }

        [Required]
        public string FirstName { get; set; }
        public string Title { get; set; }
        public string FamilyDrLastName { get; set; }
        public string FamilyDrFirstName { get; set; }
        public string FamilyDrID { get; set; }
        public string FamilyDrEmailAddress { get; set; }
        public string FamilyDrPhone { get; set; }
        public string FamilyDrExtension { get; set; }
        public string FamilyDrClinicName { get; set; }

        [ForeignKey("Insurance"), Required]
        public int InsuranceCardID { get; set; }
        public virtual Insurance Insurance { get; set; }

        [Required]
        public DateTime BirthDate { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Region { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }

        [Required]
        public string CellPhone { get; set; }
        public string HomePhone { get; set; }
        public string Alergy { get; set; }
        public string note { get; set; }

        public virtual ICollection<Prescription> PrescriptionsCollection { get; set; }

        public override string ToString()
        {
            return string.Format("PatientID {0}, FirstName {1}, LastName {2}, Gender {3}", PatientID, FirstName, LastName, Gender);
        }
    }
}
