﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamProject.Classes
{
    public class Globals
    {
        public static PharmacyContext ctx;

        public static ObservableCollection<Patient> patientsList;
        public static ObservableCollection<Medicine> medicinesList;
        public static ObservableCollection<Insurance> insuranceList;
        public static ObservableCollection<Payment> PaymentsList;
        public static ObservableCollection<PrescriptionItem> PrescriptionItemList;
        public static ObservableCollection<Prescription> PrescriptionList;
        public static ObservableCollection<Refill> RefillList;

        public static int[] excelPdfDlgAction;
        public static string excelPdfFileName;
        public static Microsoft.Office.Interop.Excel.Worksheet[] collection;

        public static DataTable dtReport;
        public static DataTable dtSearch;
    }
}
