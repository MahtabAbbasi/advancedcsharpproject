﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamProject
{
    public class Payment
    {
        [Key]
        public int PaymentID { get; set; }

        public double PaymentAmount { get; set; }

        public DateTime PaymentDate { get; set; }

        public string PaymentType { get; set; }

        [ForeignKey("Refill")]
        public int RefillId { get; set; }
        public virtual Refill Refill { get; set; }
    }
}