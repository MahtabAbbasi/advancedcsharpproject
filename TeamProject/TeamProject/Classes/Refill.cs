﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamProject
{
    public class Refill
    {
        [Key]
        public int RefillID { get; set; }
        
        public DateTime RefillDate { get; set; }

        [ForeignKey("PrescriptionItem"), Required]
        public int PrescriptionItemID { get; set; }
        public virtual PrescriptionItem PrescriptionItem { get; set; }

        public string note { get; set; }

        public virtual ICollection<Payment> PaymentCollection { get; set; }
    }
}
