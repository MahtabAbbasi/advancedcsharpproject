﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamProject
{
    public class PrescriptionItem
    {
        [Key]
        public int PrescriptionItemID { get; set; }

        [ForeignKey("Prescription")]
        public int PrescriptionID { get; set; }
        public virtual Prescription Prescription { get; set; }

        [ForeignKey("Medicine"), Required]
        public int MedicineID { get; set; }
        public virtual Medicine Medicine { get; set; }
        public string MedicineName { get; set; }

        public virtual ICollection<Refill> RefikksCollection { get; set; }

        [Required]
        public int Quantity { get; set; }

        [Required]
        public int NumberOfRefills { get; set; }
        public int RefillsDone { get; set; }

        public int RefillTimes { get; set; }
        public string Instructions { get; set; }

        public override string ToString()
        {
            return string.Format(" {0},  {1}", MedicineID, Medicine.MedicineName);
        }
	}
}
