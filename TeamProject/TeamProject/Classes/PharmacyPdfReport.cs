﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.DocumentObjectModel.Shapes;
using TeamProject.Classes;

namespace TeamProject
{
    /// <summary>
    /// Creates the report document.
    /// </summary>
    class PharmacyPdfReport
    {
        /// <summary>
        /// The MigraDoc document that represents the report.
        /// </summary>
        Document document;

        Section section;

        /// <summary>
        /// The text frame of the MigraDoc document that contains the address.
        /// </summary>
        TextFrame addressFrame;

        /// <summary>
        /// The table of the MigraDoc document that contains the report items.
        /// </summary>
        Table table;

        /// <summary>
        /// Initializes a new instance of the class Medicine report.
        /// </summary>
        public PharmacyPdfReport()
        {
        }

        /// <summary>
        /// Creates the report document.
        /// </summary>
        public Document CreateDocument()
        {
            // Create a new MigraDoc document
            this.document = new Document();
            this.document.Info.Title = "The Pharmacy report";
            this.document.Info.Subject = "Create Pharmacy report for pdf file";
            this.document.Info.Author = "Mahtab-Aziz";

            DefineStyles();

            // Each MigraDoc document needs at least one section.
            section = this.document.AddSection();

            // Put a logo in the header
            Image image = section.Headers.Primary.AddImage("../../image/canadian-pharmacy-logo.jpg");
            image.Height = "2.5cm";
            image.LockAspectRatio = true;
            image.RelativeVertical = RelativeVertical.Line;
            image.RelativeHorizontal = RelativeHorizontal.Margin;
            image.Top = ShapePosition.Top;
            image.Left = ShapePosition.Right;
            image.WrapFormat.Style = WrapStyle.Through;

            // Create footer
            Paragraph paragraph = section.Footers.Primary.AddParagraph();
            paragraph.AddText("Pharmacy Management System");
            paragraph.Format.Font.Size = 9;
            paragraph.Format.Alignment = ParagraphAlignment.Center;

            // Create the text frame for the address
            this.addressFrame = section.AddTextFrame();
            this.addressFrame.Height = "2.0cm";
            this.addressFrame.Width = "7.0cm";
            this.addressFrame.Left = ShapePosition.Left;
            this.addressFrame.RelativeHorizontal = RelativeHorizontal.Margin;
            this.addressFrame.Top = "5.0cm";
            this.addressFrame.RelativeVertical = RelativeVertical.Page;

            // Put sender in address frame
            paragraph = this.addressFrame.AddParagraph("Pharmacy Management System");
            paragraph.Format.Font.Name = "Times New Roman";
            paragraph.Format.Font.Size = 7;
            paragraph.Format.SpaceAfter = 3;

            // Fill address in address text frame
            paragraph = this.addressFrame.AddParagraph();
            paragraph.AddText("Montreal, QC");
            paragraph.AddLineBreak();

            if (Globals.excelPdfDlgAction[0] == 1)
            {
                PatientCreatePage();

                PatientFillContent();

                FamilyDrCreatePage();

                FamilyDrFillContent();
            }

            if (Globals.excelPdfDlgAction[1] == 1)
            {
                PrescriptionCreatePage();

                PrescriptionFillContent();
            }

            if (Globals.excelPdfDlgAction[2] == 1)
            {
                PrescriptionItemsCreatePage();

                PrescriptionItemsFillContent();
            }

            if (Globals.excelPdfDlgAction[3] == 1)
            {
                RefillCreatePage();

                RefillFillContent();
            }

            if (Globals.excelPdfDlgAction[4] == 1)
            {
                MedicineCreatePage();

                MedicineFillContent();
            }

            if (Globals.excelPdfDlgAction[5] == 1)
            {
                PaymentCreatePage();

                PaymentFillContent();
            }

            if (Globals.excelPdfDlgAction[6] == 1)
            {
                InsuranceCreatePage();

                InsuranceFillContent();
            }

            // Add the notes paragraph
            paragraph = this.document.LastSection.AddParagraph();
            paragraph.Format.SpaceBefore = "1cm";
            paragraph.Format.Borders.Width = 0.75;
            paragraph.Format.Borders.Distance = 3;
            paragraph.Format.Borders.Color = TableBorder;
            paragraph.Format.Shading.Color = TableGray;
            paragraph.AddText("Notes:");

            return this.document;
        }

        /// <summary>
        /// Defines the styles used to format the MigraDoc document.
        /// </summary>
        void DefineStyles()
        {
            // Get the predefined style Normal.
            Style style = this.document.Styles["Normal"];

            // Because all styles are derived from Normal, the next line changes the 
            // font of the whole document. Or, more exactly, it changes the font of
            // all styles and paragraphs that do not redefine the font.
            style.Font.Name = "Verdana";

            style = this.document.Styles[StyleNames.Header];
            style.ParagraphFormat.AddTabStop("16cm", TabAlignment.Right);

            style = this.document.Styles[StyleNames.Footer];
            style.ParagraphFormat.AddTabStop("8cm", TabAlignment.Center);

            // Create a new style called Table based on style Normal
            style = this.document.Styles.AddStyle("Table", "Normal");
            style.Font.Name = "Verdana";
            style.Font.Name = "Times New Roman";
            style.Font.Size = 9;

            // Create a new style called Reference based on style Normal
            style = this.document.Styles.AddStyle("Reference", "Normal");
            style.ParagraphFormat.SpaceBefore = "5mm";
            style.ParagraphFormat.SpaceAfter = "5mm";
            style.ParagraphFormat.TabStops.AddTabStop("16cm", TabAlignment.Right);
        }

        /// <summary>
        /// Creates the static parts of the invoice.
        /// </summary>
        void PatientCreatePage()
        {
            // Add the print date field
            Paragraph paragraph = section.AddParagraph();
            paragraph.Format.SpaceBefore = "5cm";
            paragraph.Style = "Reference";
            paragraph.AddFormattedText("Patients Information", TextFormat.Bold);
            paragraph.AddTab();
            paragraph.AddText("Created: ");
            paragraph.AddDateField("dd.MM.yyyy");

            // Create the item table
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Borders.Color = TableBorder;
            this.table.Borders.Width = 0.25;
            this.table.Borders.Left.Width = 0.5;
            this.table.Borders.Right.Width = 0.5;
            this.table.Rows.LeftIndent = 0;

            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("2.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("2.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

        // Create the header of the table
        Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Shading.Color = TableBlue;
            row.Cells[0].AddParagraph("Id");
            row.Cells[0].Format.Font.Bold = false;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].AddParagraph("First Name");
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[2].AddParagraph("Last Name");
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[3].AddParagraph("Gender");
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[4].AddParagraph("Insurance Card Id");
            row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[4].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[5].AddParagraph("Date of Birth");
            row.Cells[5].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[6].AddParagraph("Adress");
            row.Cells[6].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[6].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[7].AddParagraph("Home Phone No.");
            row.Cells[7].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[7].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[8].AddParagraph("Cell Phone No.");
            row.Cells[8].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[8].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[9].AddParagraph("Note");
            row.Cells[9].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[9].VerticalAlignment = VerticalAlignment.Center;

            this.table.SetEdge(0, 0, 10, 1, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);
        }

        /// <summary>
        /// Creates the dynamic parts of the invoice.
        /// </summary>
        void PatientFillContent()
        {
            // Iterate the invoice items
            foreach (Patient p in Globals.patientsList)
            {
                Row row = table.AddRow();
                row.HeadingFormat = true;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Format.Font.Bold = true;
                row.Shading.Color = TableBlue;
                row.Cells[0].AddParagraph(p.PatientID.ToString() + "");
                row.Cells[0].Format.Font.Bold = false;
                row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[1].AddParagraph(p.FirstName);
                row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[2].AddParagraph(p.LastName);
                row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[3].AddParagraph(p.Gender.ToString());
                row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[4].AddParagraph(p.InsuranceCardID.ToString());
                row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[4].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[5].AddParagraph(p.BirthDate.ToString("dd-mm-yyyy") + "");
                row.Cells[5].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[6].AddParagraph(p.Address + ", " + p.City + ", " + p.Region + " " + p.PostalCode);
                row.Cells[6].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[6].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[7].AddParagraph(p.HomePhone);
                row.Cells[7].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[7].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[8].AddParagraph(p.CellPhone);
                row.Cells[8].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[8].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[9].AddParagraph(p.note);
                row.Cells[9].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[9].VerticalAlignment = VerticalAlignment.Center;

                this.table.SetEdge(0, this.table.Rows.Count - 1, 10, 1, Edge.Box, BorderStyle.Single, 0.75);
            }
        }


        /// <summary>
        /// Creates the static parts of the invoice.
        /// </summary>
        void FamilyDrCreatePage()
        {
            // Add the print date field
            Paragraph paragraph = section.AddParagraph();
            paragraph.Format.SpaceBefore = "5cm";
            paragraph.Style = "Reference";
            paragraph.AddFormattedText("Family Dr Information", TextFormat.Bold);
            paragraph.AddTab();
            paragraph.AddText("Created: ");
            paragraph.AddDateField("dd.MM.yyyy");

            // Create the item table
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Borders.Color = TableBorder;
            this.table.Borders.Width = 0.25;
            this.table.Borders.Left.Width = 0.5;
            this.table.Borders.Right.Width = 0.5;
            this.table.Rows.LeftIndent = 0;

            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("2.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("2.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // Create the header of the table
            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Shading.Color = TableBlue;
            row.Cells[0].AddParagraph("Patient Id");
            row.Cells[0].Format.Font.Bold = false;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].AddParagraph("Patient's First Name");
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[2].AddParagraph("Patient's Last Name");
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[3].AddParagraph("Dr's First Name");
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[4].AddParagraph("Dr's Last Name");
            row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[4].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[5].AddParagraph("Dr's Id");
            row.Cells[5].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[6].AddParagraph("Email");
            row.Cells[6].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[6].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[7].AddParagraph("Phone No.");
            row.Cells[7].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[7].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[8].AddParagraph("Extension");
            row.Cells[8].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[8].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[9].AddParagraph("Clinic Name");
            row.Cells[9].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[9].VerticalAlignment = VerticalAlignment.Center;

            this.table.SetEdge(0, 0, 10, 1, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);
        }

        /// <summary>
        /// Creates the dynamic parts of the invoice.
        /// </summary>
        void FamilyDrFillContent()
        {
            // Iterate the invoice items
            foreach (Patient p in Globals.patientsList)
            {
                Row row = table.AddRow();
                row.HeadingFormat = true;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Format.Font.Bold = true;
                row.Shading.Color = TableBlue;
                row.Cells[0].AddParagraph(p.PatientID.ToString() + "");
                row.Cells[0].Format.Font.Bold = false;
                row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[1].AddParagraph(p.FirstName);
                row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[2].AddParagraph(p.LastName);
                row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[3].AddParagraph(p.FamilyDrFirstName);
                row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[4].AddParagraph(p.FamilyDrLastName);
                row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[4].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[5].AddParagraph(p.FamilyDrID);
                row.Cells[5].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[6].AddParagraph(p.FamilyDrEmailAddress);
                row.Cells[6].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[6].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[7].AddParagraph(p.FamilyDrPhone);
                row.Cells[7].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[7].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[8].AddParagraph(p.FamilyDrExtension);
                row.Cells[8].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[8].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[9].AddParagraph(p.FamilyDrClinicName);
                row.Cells[9].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[9].VerticalAlignment = VerticalAlignment.Center;

                this.table.SetEdge(0, this.table.Rows.Count - 1, 10, 1, Edge.Box, BorderStyle.Single, 0.75);
            }
        }

        /// <summary>
        /// Creates the static parts of the invoice.
        /// </summary>
        void PrescriptionCreatePage()
        {
            // Add the print date field
            Paragraph paragraph = section.AddParagraph();
            paragraph.Format.SpaceBefore = "5cm";
            paragraph.Style = "Reference";
            paragraph.AddFormattedText("Prescriptions Information", TextFormat.Bold);
            paragraph.AddTab();
            paragraph.AddText("Created: ");
            paragraph.AddDateField("dd.MM.yyyy");

            // Create the item table
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Borders.Color = TableBorder;
            this.table.Borders.Width = 0.25;
            this.table.Borders.Left.Width = 0.5;
            this.table.Borders.Right.Width = 0.5;
            this.table.Rows.LeftIndent = 0;

            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("2.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // Create the header of the table
            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Shading.Color = TableBlue;
            row.Cells[0].AddParagraph("Id");
            row.Cells[0].Format.Font.Bold = false;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].AddParagraph("Patient Id");
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[2].AddParagraph("Prescription Date");
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[3].AddParagraph("Dr's First Name");
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[4].AddParagraph("Dr's Last Name");
            row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[4].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[5].AddParagraph("Dr's Id");
            row.Cells[5].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[6].AddParagraph("Email");
            row.Cells[6].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[6].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[7].AddParagraph("Phone No.");
            row.Cells[7].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[7].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[8].AddParagraph("Extension");
            row.Cells[8].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[8].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[9].AddParagraph("Clinic Name");
            row.Cells[9].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[9].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[10].AddParagraph("Total Payment");
            row.Cells[10].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[10].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[11].AddParagraph("Note");
            row.Cells[11].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[11].VerticalAlignment = VerticalAlignment.Center;

            this.table.SetEdge(0, 0, 12, 1, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);
        }

        /// <summary>
        /// Creates the dynamic parts of the invoice.
        /// </summary>
        void PrescriptionFillContent()
        {
            // Iterate the invoice items
            foreach (Prescription p in Globals.PrescriptionList)
            {
                Row row = table.AddRow();
                row.HeadingFormat = true;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Format.Font.Bold = true;
                row.Shading.Color = TableBlue;
                row.Cells[0].AddParagraph(p.PrescriptionID.ToString() + "");
                row.Cells[0].Format.Font.Bold = false;
                row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[1].AddParagraph(p.PatientID.ToString() + "");
                row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[2].AddParagraph(p.Prescriptiondate.ToString("dd-mm-yyyy") + "");
                row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[3].AddParagraph(p.DrFirstName);
                row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[4].AddParagraph(p.DrLastName);
                row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[4].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[5].AddParagraph(p.DrID);
                row.Cells[5].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[6].AddParagraph(p.DrEmailAddress);
                row.Cells[6].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[6].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[7].AddParagraph(p.DrPhone);
                row.Cells[7].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[7].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[8].AddParagraph(p.DrExtension);
                row.Cells[8].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[8].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[9].AddParagraph(p.DrClinicName);
                row.Cells[9].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[9].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[10].AddParagraph(p.TotalPayment.ToString("0:0.00"));
                row.Cells[10].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[10].VerticalAlignment = VerticalAlignment.Center;
                string strNote = "";
                if (!(p.note is null))
                    strNote = p.note;
                row.Cells[11].AddParagraph(strNote);
                row.Cells[11].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[11].VerticalAlignment = VerticalAlignment.Center;

                this.table.SetEdge(0, this.table.Rows.Count - 1, 12, 1, Edge.Box, BorderStyle.Single, 0.75);
            }
        }

        /// <summary>
        /// Creates the static parts of the invoice.
        /// </summary>
        void PrescriptionItemsCreatePage()
        {
            // Add the print date field
            Paragraph paragraph = section.AddParagraph();
            paragraph.Format.SpaceBefore = "5cm";
            paragraph.Style = "Reference";
            paragraph.AddFormattedText("Prescription Items Information", TextFormat.Bold);
            paragraph.AddTab();
            paragraph.AddText("Created: ");
            paragraph.AddDateField("dd.MM.yyyy");

            // Create the item table
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Borders.Color = TableBorder;
            this.table.Borders.Width = 0.25;
            this.table.Borders.Left.Width = 0.5;
            this.table.Borders.Right.Width = 0.5;
            this.table.Rows.LeftIndent = 0;

            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("4.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // Create the header of the table
            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Shading.Color = TableBlue;
            row.Cells[0].AddParagraph("Id");
            row.Cells[0].Format.Font.Bold = false;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].AddParagraph("Prescription Id");
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[2].AddParagraph("Medicine Id");
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[3].AddParagraph("Quantity");
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[4].AddParagraph("Number Of Refills");
            row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[4].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[5].AddParagraph("Refills Done");
            row.Cells[5].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[6].AddParagraph("Refill Times");
            row.Cells[6].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[6].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[7].AddParagraph("Instructions");
            row.Cells[7].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[7].VerticalAlignment = VerticalAlignment.Center;

            this.table.SetEdge(0, 0, 8, 1, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);
        }

        /// <summary>
        /// Creates the dynamic parts of the invoice.
        /// </summary>
        void PrescriptionItemsFillContent()
        {
            // Iterate the invoice items
            foreach (PrescriptionItem p in Globals.PrescriptionItemList)
            {
                Row row = table.AddRow();
                row.HeadingFormat = true;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Format.Font.Bold = true;
                row.Shading.Color = TableBlue;
                row.Cells[0].AddParagraph(p.PrescriptionItemID.ToString() + "");
                row.Cells[0].Format.Font.Bold = false;
                row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[1].AddParagraph(p.PrescriptionID.ToString() + "");
                row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[2].AddParagraph(p.MedicineID.ToString() + "");
                row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[3].AddParagraph(p.Quantity.ToString() + "");
                row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[4].AddParagraph(p.NumberOfRefills.ToString() + "");
                row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[4].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[5].AddParagraph(p.RefillsDone.ToString() + "");
                row.Cells[5].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[6].AddParagraph(p.RefillTimes.ToString() + "");
                row.Cells[6].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[6].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[7].AddParagraph(p.Instructions);
                row.Cells[7].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[7].VerticalAlignment = VerticalAlignment.Center;
                //string strNote = "";
                //if (!(p.note is null))
                //    strNote = p.note;

                this.table.SetEdge(0, this.table.Rows.Count - 1, 8, 1, Edge.Box, BorderStyle.Single, 0.75);
            }
        }

        /// <summary>
        /// Creates the static parts of the invoice.
        /// </summary>
        void RefillCreatePage()
        {
            // Add the print date field
            Paragraph paragraph = section.AddParagraph();
            paragraph.Format.SpaceBefore = "5cm";
            paragraph.Style = "Reference";
            paragraph.AddFormattedText("Refills Information", TextFormat.Bold);
            paragraph.AddTab();
            paragraph.AddText("Created: ");
            paragraph.AddDateField("dd.MM.yyyy");

            // Create the item table
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Borders.Color = TableBorder;
            this.table.Borders.Width = 0.25;
            this.table.Borders.Left.Width = 0.5;
            this.table.Borders.Right.Width = 0.5;
            this.table.Rows.LeftIndent = 0;

            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("4cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("2.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("6cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // Create the header of the table
            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Shading.Color = TableBlue;
            row.Cells[0].AddParagraph("Id");
            row.Cells[0].Format.Font.Bold = false;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].AddParagraph("Refill Date");
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[2].AddParagraph("Prescription Item Id");
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[3].AddParagraph("Note");
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;

            this.table.SetEdge(0, 0, 4, 1, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);
        }

        /// <summary>
        /// Creates the dynamic parts of the invoice.
        /// </summary>
        void RefillFillContent()
        {
            // Iterate the invoice items
            foreach (Refill r in Globals.RefillList)
            {
                Row row = table.AddRow();
                row.HeadingFormat = true;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Format.Font.Bold = true;
                row.Shading.Color = TableBlue;
                row.Cells[0].AddParagraph(r.RefillID.ToString() + "");
                row.Cells[0].Format.Font.Bold = false;
                row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[1].AddParagraph(r.RefillDate.ToString("dd.MM.yyyy") + "");
                row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[2].AddParagraph(r.PrescriptionItemID.ToString() + "");
                row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[3].AddParagraph(r.note);
                row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[3].VerticalAlignment = VerticalAlignment.Center;

                this.table.SetEdge(0, this.table.Rows.Count - 1, 4, 1, Edge.Box, BorderStyle.Single, 0.75);
            }
        }

        /// <summary>
        /// Creates the static parts of the invoice.
        /// </summary>
        void MedicineCreatePage()
        {
            // Add the print date field
            Paragraph paragraph = section.AddParagraph();
            paragraph.Format.SpaceBefore = "5cm";
            paragraph.Style = "Reference";
            paragraph.AddFormattedText("Medicines Information", TextFormat.Bold);
            paragraph.AddTab();
            paragraph.AddText("Created: ");
            paragraph.AddDateField("dd.MM.yyyy");

            // Create the item table
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Borders.Color = TableBorder;
            this.table.Borders.Width = 0.25;
            this.table.Borders.Left.Width = 0.5;
            this.table.Borders.Right.Width = 0.5;
            this.table.Rows.LeftIndent = 0;

            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("1cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("4cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("1.5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // Create the header of the table
            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Shading.Color = TableBlue;
            row.Cells[0].AddParagraph("Id");
            row.Cells[0].Format.Font.Bold = false;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].AddParagraph("Medicine Name");
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[2].AddParagraph("Dosage");
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[3].AddParagraph("Unit of Dosage");
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[4].AddParagraph("Unit");
            row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[4].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[5].AddParagraph("Unit Price");
            row.Cells[5].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[6].AddParagraph("Note");
            row.Cells[6].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[6].VerticalAlignment = VerticalAlignment.Center;

            this.table.SetEdge(0, 0, 7, 1, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);
        }

        /// <summary>
        /// Creates the dynamic parts of the invoice.
        /// </summary>
        void MedicineFillContent()
        {
            // Iterate the invoice items
            foreach (Medicine m in Globals.medicinesList)
            {
                Row row = table.AddRow();
                row.HeadingFormat = true;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Format.Font.Bold = true;
                row.Shading.Color = TableBlue;
                row.Cells[0].AddParagraph(m.MedicineID.ToString() + "");
                row.Cells[0].Format.Font.Bold = false;
                row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[1].AddParagraph(m.MedicineName);
                row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[2].AddParagraph(m.Dosage.ToString() + "");
                row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[3].AddParagraph(m.DosageUnit);
                row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[4].AddParagraph(m.Unit.ToString() + "");
                row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[4].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[5].AddParagraph(m.UnitPrice.ToString() + "");
                row.Cells[5].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[5].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[6].AddParagraph(m.Note);
                row.Cells[6].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[6].VerticalAlignment = VerticalAlignment.Center;

                this.table.SetEdge(0, this.table.Rows.Count - 1, 7, 1, Edge.Box, BorderStyle.Single, 0.75);
            }
        }

        /// <summary>
        /// Creates the static parts of the invoice.
        /// </summary>
        void PaymentCreatePage()
        {
            // Add the print date field
            Paragraph paragraph = section.AddParagraph();
            paragraph.Format.SpaceBefore = "5cm";
            paragraph.Style = "Reference";
            paragraph.AddFormattedText("Payment Information", TextFormat.Bold);
            paragraph.AddTab();
            paragraph.AddText("Created: ");
            paragraph.AddDateField("dd.MM.yyyy");

            // Create the item table
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Borders.Color = TableBorder;
            this.table.Borders.Width = 0.25;
            this.table.Borders.Left.Width = 0.5;
            this.table.Borders.Right.Width = 0.5;
            this.table.Rows.LeftIndent = 0;

            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("2cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("4cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("3cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("3cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("3cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // Create the header of the table
            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Shading.Color = TableBlue;
            row.Cells[0].AddParagraph("Id");
            row.Cells[0].Format.Font.Bold = false;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].AddParagraph("Payment Amount");
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[2].AddParagraph("Payment Date");
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[3].AddParagraph("Payment Type");
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[4].AddParagraph("Refill Id");
            row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[4].VerticalAlignment = VerticalAlignment.Center;

            this.table.SetEdge(0, 0, 5, 1, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);
        }

        /// <summary>
        /// Creates the dynamic parts of the invoice.
        /// </summary>
        void PaymentFillContent()
        {
            // Iterate the invoice items
            foreach (Payment p in Globals.PaymentsList)
            {
                Row row = table.AddRow();
                row.HeadingFormat = true;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Format.Font.Bold = true;
                row.Shading.Color = TableBlue;
                row.Cells[0].AddParagraph(p.PaymentID.ToString() + "");
                row.Cells[0].Format.Font.Bold = false;
                row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[1].AddParagraph(p.PaymentAmount.ToString("0.00") + "");
                row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[2].AddParagraph(p.PaymentDate.ToString("dd.MM.yyyy") + "");
                row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[3].AddParagraph(p.PaymentType);
                row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[3].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[4].AddParagraph(p.RefillId.ToString() + "");
                row.Cells[4].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[4].VerticalAlignment = VerticalAlignment.Center;

                this.table.SetEdge(0, this.table.Rows.Count - 1, 5, 1, Edge.Box, BorderStyle.Single, 0.75);
            }
        }

        /// <summary>
        /// Creates the static parts of the invoice.
        /// </summary>
        void InsuranceCreatePage()
        {
            // Add the print date field
            Paragraph paragraph = section.AddParagraph();
            paragraph.Format.SpaceBefore = "5cm";
            paragraph.Style = "Reference";
            paragraph.AddFormattedText("Insurance Information", TextFormat.Bold);
            paragraph.AddTab();
            paragraph.AddText("Created: ");
            paragraph.AddDateField("dd.MM.yyyy");

            // Create the item table
            this.table = section.AddTable();
            this.table.Style = "Table";
            this.table.Borders.Color = TableBorder;
            this.table.Borders.Width = 0.25;
            this.table.Borders.Left.Width = 0.5;
            this.table.Borders.Right.Width = 0.5;
            this.table.Rows.LeftIndent = 0;

            // Before you can add a row, you must define the columns
            Column column = this.table.AddColumn("3cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("4cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("5cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            column = this.table.AddColumn("4cm");
            column.Format.Alignment = ParagraphAlignment.Center;

            // Create the header of the table
            Row row = table.AddRow();
            row.HeadingFormat = true;
            row.Format.Alignment = ParagraphAlignment.Center;
            row.Format.Font.Bold = true;
            row.Shading.Color = TableBlue;
            row.Cells[0].AddParagraph("Insurance Card Id");
            row.Cells[0].Format.Font.Bold = false;
            row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[1].AddParagraph("Coverage Percent");
            row.Cells[1].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[2].AddParagraph("Insurance Company Name");
            row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
            row.Cells[3].AddParagraph("Insurance Type");
            row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
            row.Cells[3].VerticalAlignment = VerticalAlignment.Center;

            this.table.SetEdge(0, 0, 4, 1, Edge.Box, BorderStyle.Single, 0.75, Color.Empty);
        }

        /// <summary>
        /// Creates the dynamic parts of the invoice.
        /// </summary>
        void InsuranceFillContent()
        {
            // Iterate the invoice items
            foreach (Insurance i in Globals.insuranceList)
            {
                Row row = table.AddRow();
                row.HeadingFormat = true;
                row.Format.Alignment = ParagraphAlignment.Center;
                row.Format.Font.Bold = true;
                row.Shading.Color = TableBlue;
                row.Cells[0].AddParagraph(i.InsuranceCardID.ToString() + "");
                row.Cells[0].Format.Font.Bold = false;
                row.Cells[0].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[0].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[1].AddParagraph(i.CoveragePercent.ToString("0.00") + "");
                row.Cells[1].Format.Alignment = ParagraphAlignment.Left;
                row.Cells[1].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[2].AddParagraph(i.InsuranceCompanyName);
                row.Cells[2].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[2].VerticalAlignment = VerticalAlignment.Center;
                row.Cells[3].AddParagraph(i.InsuranceType);
                row.Cells[3].Format.Alignment = ParagraphAlignment.Center;
                row.Cells[3].VerticalAlignment = VerticalAlignment.Center;

                this.table.SetEdge(0, this.table.Rows.Count - 1, 4, 1, Edge.Box, BorderStyle.Single, 0.75);
            }
        }

        // Some pre-defined colors
#if true
        // RGB colors
        readonly static Color TableBorder = new Color(81, 125, 192);
        readonly static Color TableBlue = new Color(235, 240, 249);
        readonly static Color TableGray = new Color(242, 242, 242);
#else
    // CMYK colors
    readonly static Color tableBorder = Color.FromCmyk(100, 50, 0, 30);
    readonly static Color tableBlue = Color.FromCmyk(0, 80, 50, 30);
    readonly static Color tableGray = Color.FromCmyk(30, 0, 0, 0, 100);
#endif
    }
}
