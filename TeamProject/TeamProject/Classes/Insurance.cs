﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamProject
{
    public class Insurance
    {
        [Key]
        public int InsuranceCardID { get; set; }

        [Required]
        public double CoveragePercent { get; set; }

        [Required]
        public string InsuranceCompanyName { get; set; }

        [Required]
        public string InsuranceType { get; set; }

        //public virtual Patient Patient { get; set; }

        public override string ToString()
        {
            return string.Format("Insurance ID: {0}, Amount Paied: {1}, Insurance Co. Name: {2}, Insurance Type: {3}", InsuranceCardID, CoveragePercent, InsuranceCompanyName, InsuranceType);
        }
    }
}
