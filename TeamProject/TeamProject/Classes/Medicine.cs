﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamProject
{
    public class Medicine
    {
        [Key]
        public int MedicineID { get; set; }

        [MaxLength(50), Required]
        public string MedicineName { get; set; }

        [Required]
        public double Dosage { get; set; }

        [MaxLength(20), Required]
        public string Unit { get; set; }

        [Required]
        public double UnitPrice { get; set; }

        [MaxLength(150), Required]
        public string Note { get; set; }

        public string DosageUnit { get; set; }

        public virtual ICollection<PrescriptionItem> PrescriptionItemsCollections { get; set; }

        public override string ToString()
        {
            return string.Format("MedicineID {0}, MedicineName {1}, Dosage {2}, Unit {3}, UnitPrice {4}, Note {5}", MedicineID, MedicineName, Dosage, Unit, UnitPrice, Note);
        }
    }
}
