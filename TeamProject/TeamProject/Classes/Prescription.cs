﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamProject
{
    public class Prescription
    {
        [Key]
        public int PrescriptionID { get; set; }

        [ForeignKey("Patient"), Required]
        public int PatientID { get; set; }
        public virtual Patient Patient { get; set; }

        [Required]
        public DateTime Prescriptiondate { get; set; }

        public string DrLastName { get; set; }
        public string DrFirstName { get; set; }
        public string DrID { get; set; }
        public string DrEmailAddress { get; set; }
        public string DrPhone { get; set; }
        public string DrExtension { get; set; }
        public string DrClinicName { get; set; }

        [Required]
        public double TotalPayment { get; set; }
        public string note { get; set; }

        public virtual ICollection<PrescriptionItem> PrescriptionItemsCollections { get; set; }

        public override string ToString()
        {
            return string.Format(" {0},  {1},  {2},  {3}", PrescriptionID, Prescriptiondate,Patient.FirstName, Patient.LastName);
        }
	}
}
