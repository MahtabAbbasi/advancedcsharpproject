﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TeamProject.Classes;

namespace TeamProject
{
    /// <summary>
    /// Interaction logic for ExcelPDFDialog.xaml
    /// </summary>
    public partial class ExcelPDFDialog : Window
    {
        public ExcelPDFDialog(MainWindow mainWindow)
        {
            InitializeComponent();
        }

        private void ButtonCreateExcelPdf_Click(object sender, RoutedEventArgs e)
        {
            for (int i = 0; i < 7; ++i)
                Globals.excelPdfDlgAction[i] = 0;

            if (cmbPatientTable.IsChecked == true)           Globals.excelPdfDlgAction[0] = 1;
            if (cmbPrescriptionTable.IsChecked == true)      Globals.excelPdfDlgAction[1] = 1;
            if (cmbPrescriptionItemsTable.IsChecked == true) Globals.excelPdfDlgAction[2] = 1;
            if (cmbRefillTable.IsChecked == true)            Globals.excelPdfDlgAction[3] = 1;
            if (cmbMedicineTable.IsChecked == true)          Globals.excelPdfDlgAction[4] = 1;
            if (cmbPaymentTable.IsChecked == true)           Globals.excelPdfDlgAction[5] = 1;
            if (cmbInsuranceTable.IsChecked == true)         Globals.excelPdfDlgAction[6] = 1;

            Globals.excelPdfFileName = tbExcelPdfFileName.Text;
            this.DialogResult = true;
        }

        private void tbExcelPdfFileName_GotFocus(object sender, RoutedEventArgs e)
        {
            tbExcelPdfFileName.Clear();
        }
    }
}
