﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void inputPatientInfo(out string firstName, out string lastName, out Patient.GenderEnum gender, out string address, out string city, out string region, out string postalcode, 
            out string country, out string cellPhone, out string homephone, out DateTime birthDate, out string drId, out string drFirstName, out string drLastName, 
            out string clinicName, out string phoneNumber, out string Extention, out string emailAdd, out string alergy, out string note, out string companyName, out string type, out double coveragePercent, out int cardID)
        { 
            // Patient Personal Information
            firstName = tbPPFirstName.Text;

            lastName = tbPPLastName.Text;

            birthDate = Convert.ToDateTime(dpPPBirthDate.Text);

            if (!Enum.TryParse<Patient.GenderEnum>(cbxPPGender.Text, result: out gender))
                throw new InvalidDataException("The format of gender enum is not correct." + cbxPPGender.Text);


            // Patient Contact Information
            address = tbPCAddress.Text;

            city = tbPCCity.Text;

            region = tbPCRegion.Text;

            postalcode = tbPCPostalCode.Text;

            country = tbPCCountry.Text;

            cellPhone = tbPCCellPhone.Text;

            homephone = tbPCHomePhone.Text;


            // Patient Family Dr Information
            drId = tbFDIDrId.Text;

            drFirstName = tbFDIFirstName.Text;

            drLastName = tbFDILastName.Text;

            clinicName = tbFDIClinicName.Text;

            phoneNumber = tbFDIPhoneNumber.Text;

            Extention = tbFDIExt.Text;

            emailAdd = tbFDIEmailAdd.Text;


            // Patient Medical History
            alergy = "";
            if (chbMHAntibiotics.IsChecked == true) alergy += "Antibiotics,";
            if (chbMHSulfa.IsChecked == true) alergy += "Sulfa,";
            if (chbMHAspirin.IsChecked == true) alergy += "Aspirin,";
            if (chbMHInsulin.IsChecked == true) alergy += "Insulin,";
            if (alergy.EndsWith(@",")) alergy = alergy.Substring(0, alergy.Length - 1);

            note = tbMHNotes.Text;


            // Patient Insurance Information
            string strCardID = tbPICardId.Text;
            if (!int.TryParse(strCardID, out cardID))
                throw new InvalidDataException("The format of insurance card id is not correct.");

            string strcoveragePercent = tbPICoverage.Text;
            if (!Double.TryParse(strcoveragePercent, out coveragePercent))
                throw new InvalidDataException("The format of insurance coverage is not correct.");

            companyName = tbPICampanyName.Text;

            type = tbPIInsuranceType.Text;

        }
    }
}
