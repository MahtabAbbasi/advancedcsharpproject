﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void AddPatient()
        {
            Globals.patientsList = new ObservableCollection<Patient>();
            Globals.insuranceList = new ObservableCollection<Insurance>();
            Globals.ctx = new PharmacyContext();

            try
            {
                inputPatientInfo(out string firstName, out string lastName, out Patient.GenderEnum gender, out string address, out string city, out string region, out string postalcode,
                    out string country, out string cellPhone, out string homephone, out DateTime birthDate, out string drId, out string drFirstName, out string drLastName,
                    out string clinicName, out string phoneNumber, out string Extention, out string emailAdd, out string alergy, out string note, out string companyName, out string type, out double coveragePercent, out int cardID);
                Insurance insurance = new Insurance
                {
                    InsuranceCardID = cardID,
                    CoveragePercent = coveragePercent,
                    InsuranceCompanyName = companyName,
                    InsuranceType = type
                };
                Globals.ctx.InsuranceCollection.Add(insurance);
                Patient patient = new Patient
                {
                    LastName             = lastName,
                    FirstName            = firstName,
                    Gender               = gender,
                    FamilyDrLastName     = drLastName,
                    FamilyDrFirstName    = drFirstName,
                    FamilyDrID           = drId,
                    FamilyDrEmailAddress = emailAdd,
                    FamilyDrPhone        = phoneNumber,
                    FamilyDrExtension    = Extention,
                    FamilyDrClinicName   = clinicName,
                    InsuranceCardID      = cardID,
                    BirthDate            = birthDate,
                    Address              = address,
                    City                 = city,
                    Region               = region,
                    PostalCode           = postalcode,
                    Country              = country,                 
                    CellPhone            = cellPhone,
                    HomePhone            = phoneNumber,
                    Alergy               = alergy,
                    note                 = note
                };
                patient.InsuranceCardID = insurance.InsuranceCardID;
                Globals.ctx.PatientCollection.Add(patient);
                Globals.ctx.SaveChanges();
                MessageBox.Show("Medicine and Insurance added successfully");
                Globals.patientsList.Add(patient);
                Globals.insuranceList.Add(insurance);
                //refreshMedicineList();
                refreshPatientList();
            }
            catch (InvalidDataException ex)
            {
                MessageBox.Show(this, ex.Message, "Input error");
            }
            catch (NullReferenceException ex)
            {
                MessageBox.Show(this, ex.Message, "Null error");
            }
        }
    }
}
