﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void prescriptionItemsListView()
        {
            var prescriptionItems = (from me in Globals.ctx.PrescriptionItemCollection select me).ToList();
            Globals.PrescriptionItemList = new ObservableCollection<PrescriptionItem>(prescriptionItems);

            Globals.dtReport = new System.Data.DataTable();

            Globals.dtReport.Columns.Add("Prescription Item Id");
            Globals.dtReport.Columns.Add("Prescription Id");
            Globals.dtReport.Columns.Add("Medicine Id");
            Globals.dtReport.Columns.Add("Quantity");
            Globals.dtReport.Columns.Add("Number Of Refills");
            Globals.dtReport.Columns.Add("Refills Done");
            Globals.dtReport.Columns.Add("Refill Times");
            Globals.dtReport.Columns.Add("Instructions");

            foreach (PrescriptionItem p in Globals.PrescriptionItemList)
            {
                Globals.dtReport.Rows.Add(p.PrescriptionItemID.ToString(), p.PrescriptionID.ToString(), 
                    p.MedicineID.ToString(), p.Quantity.ToString("0.00"), p.NumberOfRefills.ToString(), 
                    p.RefillsDone.ToString(), p.RefillTimes.ToString(), p.Instructions);
            }
            Globals.dtReport.AcceptChanges();

            //lvReport.ItemsSource = Globals.dtReport.Rows;
            lvReport.DataContext = Globals.dtReport;

            gvReport.Columns.Clear();

            foreach (var col in Globals.dtReport.Columns)
            {
                DataColumn dc = (DataColumn)col;
                GridViewColumn column = new GridViewColumn();
                column.DisplayMemberBinding = new Binding(dc.ColumnName);
                column.Header = dc.ColumnName;
                gvReport.Columns.Add(column);

                Binding bind = new Binding();
                lvReport.SetBinding(ListView.ItemsSourceProperty, bind);
            }
        }
    }
}
