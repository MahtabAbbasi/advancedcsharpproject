﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void medicinesExcelSheet(int i)
        {
            Globals.collection[i].Cells[1, 1 ] = "Medicine Id";
            Globals.collection[i].Cells[1, 2 ] = "Medicine Name";
            Globals.collection[i].Cells[1, 3 ] = "Dosage";
            Globals.collection[i].Cells[1, 4 ] = "Unit of Dosage";
            Globals.collection[i].Cells[1, 5 ] = "Unit";
            Globals.collection[i].Cells[1, 6 ] = "Unit Price";
            Globals.collection[i].Cells[1, 7 ] = "Note";

            int j = 1;
            foreach (Medicine m in Globals.medicinesList)
            {
                j = j + 1;
                Globals.collection[i].Cells[j, 1] = m.MedicineID.ToString();
                Globals.collection[i].Cells[j, 2] = m.MedicineName;
                Globals.collection[i].Cells[j, 3] = m.Dosage.ToString();
                Globals.collection[i].Cells[j, 4] = m.DosageUnit;
                Globals.collection[i].Cells[j, 5] = m.Unit.ToString();
                Globals.collection[i].Cells[j, 6] = m.UnitPrice.ToString();
                Globals.collection[i].Cells[j, 7] = m.Note;
            }
        }
    }
}
