﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void insuranceListView()
        {
            var insurance = (from me in Globals.ctx.InsuranceCollection select me).ToList();
            Globals.insuranceList = new ObservableCollection<Insurance>(insurance);

            Globals.dtReport = new System.Data.DataTable();

            Globals.dtReport.Columns.Add("Insurance Card Id");
            Globals.dtReport.Columns.Add("Coverage Percent");
            Globals.dtReport.Columns.Add("Insurance Company Name");
            Globals.dtReport.Columns.Add("Insurance Type");

            foreach (Insurance iu in Globals.insuranceList)
            {
                Globals.dtReport.Rows.Add(iu.InsuranceCardID.ToString(), iu.CoveragePercent.ToString("0.00"),
                    iu.InsuranceCompanyName, iu.InsuranceType);
            }
            Globals.dtReport.AcceptChanges();

            //lvReport.ItemsSource = Globals.dtReport.Rows;
            lvReport.DataContext = Globals.dtReport;

            gvReport.Columns.Clear();

            foreach (var col in Globals.dtReport.Columns)
            {
                DataColumn dc = (DataColumn)col;
                GridViewColumn column = new GridViewColumn();
                column.DisplayMemberBinding = new Binding(dc.ColumnName);
                column.Header = dc.ColumnName;
                gvReport.Columns.Add(column);

                Binding bind = new Binding();
                lvReport.SetBinding(ListView.ItemsSourceProperty, bind);
            }
        }
    }
}
