﻿using MigraDoc.DocumentObjectModel;
using MigraDoc.DocumentObjectModel.IO;
using MigraDoc.DocumentObjectModel.Tables;
using MigraDoc.Rendering;
using PdfSharp;
using PdfSharp.Drawing;
using PdfSharp.Pdf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using TeamProject.Classes;
using TeamProject.Forms;
using MigraDoc;
using PdfSharp.Pdf.Content.Objects;
using MigraDoc.DocumentObjectModel.Shapes;
using ExcelLibrary.SpreadSheet;
using ExcelLibrary.CompoundDocumentFormat;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;

namespace TeamProject
{
    /// <summary>
    /// Export Information to an Excel file
    /// </summary>
    public partial class MainWindow
    {
        public void exportToExcel()
        {
            Excel.Application excel;
            Excel.Workbook excelWB;

            Globals.collection = new Microsoft.Office.Interop.Excel.Worksheet[7];

            try
            {
                //Start Excel and get Application object.
                excel = new Excel.Application();
                excel.Visible = false;

                //Get a new workbook.
                excelWB = (Excel.Workbook)(excel.Workbooks.Add(Missing.Value));

                int i = 0;
                if (Globals.excelPdfDlgAction[0] == 1)
                {
                    Globals.collection[i] = (Excel.Worksheet)excelWB.Worksheets.Add();
                    Globals.collection[i].Name = String.Format("Patients");
                    Globals.collection[i].DisplayRightToLeft = false;
                    Globals.collection[i].Columns.AutoFit();
                    Globals.collection[i].Rows.AutoFit();
                    patientsExcelSheet(i);
                }

                if (Globals.excelPdfDlgAction[1] == 1)
                {
                    i += 1;
                    Globals.collection[i] = (Excel.Worksheet)excelWB.Worksheets.Add();
                    Globals.collection[i].Name = String.Format("Prescriptions");
                    Globals.collection[i].DisplayRightToLeft = false;
                    Globals.collection[i].Columns.AutoFit();
                    Globals.collection[i].Rows.AutoFit();
                    prescriptionsExcelSheet(i);
                }

                if (Globals.excelPdfDlgAction[2] == 1)
                {
                    i += 1;
                    Globals.collection[i] = (Excel.Worksheet)excelWB.Worksheets.Add();
                    Globals.collection[i].Name = String.Format("Prescription Items");
                    Globals.collection[i].DisplayRightToLeft = false;
                    Globals.collection[i].Columns.AutoFit();
                    Globals.collection[i].Rows.AutoFit();
                    prescriptionItemsExcelSheet(i);
                }

                if (Globals.excelPdfDlgAction[3] == 1)
                {
                    i += 1;
                    Globals.collection[i] = (Excel.Worksheet)excelWB.Worksheets.Add();
                    Globals.collection[i].Name = String.Format("Refills");
                    Globals.collection[i].DisplayRightToLeft = false;
                    Globals.collection[i].Columns.AutoFit();
                    Globals.collection[i].Rows.AutoFit();
                    refillsExcelSheet(i);
                }

                if (Globals.excelPdfDlgAction[4] == 1)
                {
                    i += 1;
                    Globals.collection[i] = (Excel.Worksheet)excelWB.Worksheets.Add();
                    Globals.collection[i].Name = String.Format("Medicine");
                    Globals.collection[i].DisplayRightToLeft = false;
                    Globals.collection[i].Columns.AutoFit();
                    Globals.collection[i].Rows.AutoFit();
                    medicinesExcelSheet(i);
                }

                if (Globals.excelPdfDlgAction[5] == 1)
                {
                    i += 1;
                    Globals.collection[i] = (Excel.Worksheet)excelWB.Worksheets.Add();
                    Globals.collection[i].Name = String.Format("Payments");
                    Globals.collection[i].DisplayRightToLeft = false;
                    Globals.collection[i].Columns.AutoFit();
                    Globals.collection[i].Rows.AutoFit();
                    paymentsExcelSheet(i);
                }


                if (Globals.excelPdfDlgAction[6] == 1)
                {
                    i += 1;
                    Globals.collection[i] = (Excel.Worksheet)excelWB.Worksheets.Add();
                    Globals.collection[i].Name = String.Format("Insurance");
                    Globals.collection[i].DisplayRightToLeft = false;
                    Globals.collection[i].Columns.AutoFit();
                    Globals.collection[i].Rows.AutoFit();
                    insuranceExcelSheet(i);
                }

                var file = Environment.CurrentDirectory + @"/../../" + Globals.excelPdfFileName + ".xlsx";
                excelWB.SaveAs(file);
                excelWB.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Couldn't create Excel file.\r\nException: " + ex.Message);
            }
        }
    }

}
