﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void SelectedMedicinesListView()
        {
            Globals.dtSearch = new System.Data.DataTable();

            Globals.dtSearch.Columns.Add("Medicine Id");
            Globals.dtSearch.Columns.Add("Medicine Name");
            Globals.dtSearch.Columns.Add("Dosage");
            Globals.dtSearch.Columns.Add("Unit of Dosage");
            Globals.dtSearch.Columns.Add("Unit");
            Globals.dtSearch.Columns.Add("Unit Price");
            Globals.dtSearch.Columns.Add("Note");

            foreach (Medicine m in Globals.medicinesList)
            {
                Globals.dtSearch.Rows.Add(m.MedicineID.ToString(), m.MedicineName, m.Dosage.ToString(), 
                    m.DosageUnit, m.Unit.ToString(), m.UnitPrice.ToString(), m.Note);
            }
            Globals.dtSearch.AcceptChanges();

            //lvSearch.ItemsSource = Globals.dtSearch.Rows;
            lvSearch.DataContext = Globals.dtSearch;

            gvSearch.Columns.Clear();

            foreach (var col in Globals.dtSearch.Columns)
            {
                DataColumn dc = (DataColumn)col;
                GridViewColumn column = new GridViewColumn();
                column.DisplayMemberBinding = new Binding(dc.ColumnName);
                column.Header = dc.ColumnName;
                gvSearch.Columns.Add(column);

                Binding bind = new Binding();
                lvSearch.SetBinding(ListView.ItemsSourceProperty, bind);
            }
        }
    }
}
