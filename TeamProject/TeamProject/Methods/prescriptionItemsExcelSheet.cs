﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void prescriptionItemsExcelSheet(int i)
        {
            Globals.collection[i].Cells[1, 1 ] = "Prescription Item Id";
            Globals.collection[i].Cells[1, 2 ] = "Prescription Id";
            Globals.collection[i].Cells[1, 3 ] = "Medicine Id";
            Globals.collection[i].Cells[1, 4 ] = "Quantity";
            Globals.collection[i].Cells[1, 5 ] = "Number Of Refills";
            Globals.collection[i].Cells[1, 6 ] = "Refills Done";
            Globals.collection[i].Cells[1, 7 ] = "Refill Times";
            Globals.collection[i].Cells[1, 8 ] = "Instructions";

            int j = 1;
            foreach (PrescriptionItem p in Globals.PrescriptionItemList)
            {
                j = j + 1;
                Globals.collection[i].Cells[j, 1] = p.PrescriptionItemID.ToString();
                Globals.collection[i].Cells[j, 2] = p.PrescriptionID.ToString();
                Globals.collection[i].Cells[j, 3] = p.MedicineID.ToString();
                Globals.collection[i].Cells[j, 4] = p.Quantity.ToString("0.00");
                Globals.collection[i].Cells[j, 5] = p.NumberOfRefills.ToString();
                Globals.collection[i].Cells[j, 6] = p.RefillsDone.ToString();
                Globals.collection[i].Cells[j, 7] = p.RefillTimes.ToString();
                Globals.collection[i].Cells[j, 8] = p.Instructions;
            }
        }
    }
}
