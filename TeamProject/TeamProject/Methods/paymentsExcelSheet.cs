﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void paymentsExcelSheet(int i)
        {
            Globals.collection[i].Cells[1, 1 ] = "Payment Id";
            Globals.collection[i].Cells[1, 2 ] = "Payment Amount";
            Globals.collection[i].Cells[1, 3 ] = "Payment Date";
            Globals.collection[i].Cells[1, 4 ] = "Payment Type";
            Globals.collection[i].Cells[1, 5 ] = "Refill Id";

            int j = 1;
            foreach (Payment p in Globals.PaymentsList)
            {
                j = j + 1;
                Globals.collection[i].Cells[j, 1] = p.PaymentID.ToString();
                Globals.collection[i].Cells[j, 2] = p.PaymentAmount.ToString("0.00");
                Globals.collection[i].Cells[j, 3] = p.PaymentDate.ToString("dd.MM.yyyy");
                Globals.collection[i].Cells[j, 4] = p.PaymentType;
                Globals.collection[i].Cells[j, 5] = p.RefillId.ToString();
            }
        }
    }
}
