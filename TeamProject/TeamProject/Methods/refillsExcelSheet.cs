﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void refillsExcelSheet(int i)
        {
            Globals.collection[i].Cells[1, 1 ] = "Refill Id";
            Globals.collection[i].Cells[1, 2 ] = "Refill Date";
            Globals.collection[i].Cells[1, 3 ] = "Prescription Item Id";
            Globals.collection[i].Cells[1, 4 ] = "Note";

            int j = 1;
            foreach (Refill r in Globals.RefillList)
            {
                j = j + 1;
                Globals.collection[i].Cells[j, 1] = r.RefillID.ToString();
                Globals.collection[i].Cells[j, 2] = r.RefillDate.ToString("dd.MM.yyyy");
                Globals.collection[i].Cells[j, 3] = r.PrescriptionItemID.ToString();
                Globals.collection[i].Cells[j, 4] = r.note;
            }
        }
    }
}
