﻿using MigraDoc.Rendering;
using System;
using System.Diagnostics;
using TeamProject.Classes;
using MigraDoc.Rendering.Printing;

namespace TeamProject
{
    /// <summary>
    /// Export Information to an Pdf file
    /// </summary>
    public partial class MainWindow
    {
        public void printDocument()
        {
            try
            {
                // Create a invoice form with the sample invoice data
                PharmacyPdfReport report = new PharmacyPdfReport();

                // Create a MigraDoc document
                MigraDoc.DocumentObjectModel.Document document = report.CreateDocument();
                document.UseCmykColor = true;

#if DEBUG
                // for debugging only...
                MigraDoc.DocumentObjectModel.IO.DdlWriter.WriteToFile(document, "MigraDoc.mdddl");
                document = MigraDoc.DocumentObjectModel.IO.DdlReader.DocumentFromFile("MigraDoc.mdddl");
#endif

                //// Create a renderer for PDF that uses Unicode font encoding
                //MigraDoc.Rendering.PdfDocumentRenderer pdfRenderer = new MigraDoc.Rendering.PdfDocumentRenderer(true);

                //// Set the MigraDoc document
                //pdfRenderer.Document = document;

                //// Create the PDF document
                //pdfRenderer.RenderDocument();

                //// Save the PDF document...
                //string filename = Environment.CurrentDirectory + @"/../../" + Globals.excelPdfFileName + ".pdf";
                ////#if DEBUG
                ////                    // I don't want to close the document constantly...
                ////                    filename = "Pharmacy-" + Guid.NewGuid().ToString("N").ToUpper() + ".pdf";
                ////#endif
                //pdfRenderer.Save(filename);

                MigraDocPrintDocument printDocument = new MigraDocPrintDocument();
                printDocument.Renderer = new DocumentRenderer(document);
                printDocument.Renderer.PrepareDocument();
                printDocument.Print();

                //// ...and start a viewer.
                //Process.Start(filename);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadLine();
            }
        }
    }
}
