﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void SelectedPatientsListView()
        {
            Globals.dtSearch = new System.Data.DataTable();

            Globals.dtSearch.Columns.Add("Patient Id");
            Globals.dtSearch.Columns.Add("Name");
            Globals.dtSearch.Columns.Add("Gender");
            Globals.dtSearch.Columns.Add("Insurance Card Id");
            Globals.dtSearch.Columns.Add("Date of Birth");
            Globals.dtSearch.Columns.Add("Adress");
            Globals.dtSearch.Columns.Add("Home Phone No.");
            Globals.dtSearch.Columns.Add("Cell Phone No.");
            Globals.dtSearch.Columns.Add("Dr's Name");
            Globals.dtSearch.Columns.Add("Dr's Id");
            Globals.dtSearch.Columns.Add("Email");
            Globals.dtSearch.Columns.Add("Phone No.");
            Globals.dtSearch.Columns.Add("Extension");
            Globals.dtSearch.Columns.Add("Clinic Name");
            Globals.dtSearch.Columns.Add("Note");

            foreach (Patient p in Globals.patientsList)
            {
                string name = p.FirstName + " " + p.LastName;
                string address = p.Address + ", " + p.City + ", " + p.Region + " " + p.PostalCode;
                string drName = p.FamilyDrFirstName + " " + p.FamilyDrLastName;
                Globals.dtSearch.Rows.Add(p.PatientID.ToString(), name, p.Gender.ToString(), p.InsuranceCardID.ToString(),
                                       p.BirthDate.ToString("dd/mm/yyyy"), address, p.HomePhone, p.CellPhone, drName,
                                       p.FamilyDrID.ToString(), p.FamilyDrEmailAddress, p.FamilyDrPhone, 
                                       p.FamilyDrExtension, p.FamilyDrClinicName, p.note);
            }
            Globals.dtSearch.AcceptChanges();

            lvSearch.DataContext = Globals.dtSearch;

            gvSearch.Columns.Clear();

            foreach (var col in Globals.dtSearch.Columns)
            {
                DataColumn dc = (DataColumn)col;
                GridViewColumn column = new GridViewColumn();
                column.DisplayMemberBinding = new Binding(dc.ColumnName);
                column.Header = dc.ColumnName;
                gvSearch.Columns.Add(column);

                Binding bind = new Binding();
                lvSearch.SetBinding(ListView.ItemsSourceProperty, bind);
            }
        }
    }
}
