﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void SelectedPrescriptionItemsListView()
        {
            Globals.dtSearch = new System.Data.DataTable();

            Globals.dtSearch.Columns.Add("Prescription Item Id");
            Globals.dtSearch.Columns.Add("Prescription Id");
            Globals.dtSearch.Columns.Add("Medicine Id");
            Globals.dtSearch.Columns.Add("Quantity");
            Globals.dtSearch.Columns.Add("Number Of Refills");
            Globals.dtSearch.Columns.Add("Refills Done");
            Globals.dtSearch.Columns.Add("Refill Times");
            Globals.dtSearch.Columns.Add("Instructions");

            foreach (PrescriptionItem p in Globals.PrescriptionItemList)
            {
                Globals.dtSearch.Rows.Add(p.PrescriptionItemID.ToString(), p.PrescriptionID.ToString(), 
                    p.MedicineID.ToString(), p.Quantity.ToString("0.00"), p.NumberOfRefills.ToString(), 
                    p.RefillsDone.ToString(), p.RefillTimes.ToString(), p.Instructions);
            }
            Globals.dtSearch.AcceptChanges();

            //lvSearch.ItemsSource = Globals.dtSearch.Rows;
            lvSearch.DataContext = Globals.dtSearch;

            gvSearch.Columns.Clear();

            foreach (var col in Globals.dtSearch.Columns)
            {
                DataColumn dc = (DataColumn)col;
                GridViewColumn column = new GridViewColumn();
                column.DisplayMemberBinding = new Binding(dc.ColumnName);
                column.Header = dc.ColumnName;
                gvSearch.Columns.Add(column);

                Binding bind = new Binding();
                lvSearch.SetBinding(ListView.ItemsSourceProperty, bind);
            }
        }
    }
}
