﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void insuranceExcelSheet(int i)
        {
            Globals.collection[i].Cells[1, 1 ] = "Insurance Card Id";
            Globals.collection[i].Cells[1, 2 ] = "Coverage Percent";
            Globals.collection[i].Cells[1, 3 ] = "Insurance Company Name";
            Globals.collection[i].Cells[1, 4 ] = "Insurance Type";

            int j = 1;
            foreach (Insurance iu in Globals.insuranceList)
            {
                j = j + 1;
                Globals.collection[i].Cells[j, 1] = iu.InsuranceCardID.ToString();
                Globals.collection[i].Cells[j, 2] = iu.CoveragePercent.ToString("0.00");
                Globals.collection[i].Cells[j, 3] = iu.InsuranceCompanyName;
                Globals.collection[i].Cells[j, 4] = iu.InsuranceType;
            }
        }
    }
}
