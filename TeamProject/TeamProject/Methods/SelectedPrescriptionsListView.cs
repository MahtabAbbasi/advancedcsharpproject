﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void SelectedPrescriptionsListView()
        {
            Globals.dtSearch = new System.Data.DataTable();

            Globals.dtSearch.Columns.Add("Prescription Id");
            Globals.dtSearch.Columns.Add("Patient Id");
            Globals.dtSearch.Columns.Add("Prescription Date");
            Globals.dtSearch.Columns.Add("Dr's Name");
            Globals.dtSearch.Columns.Add("Dr's Id");
            Globals.dtSearch.Columns.Add("Email");
            Globals.dtSearch.Columns.Add("Phone No.");
            Globals.dtSearch.Columns.Add("Extension");
            Globals.dtSearch.Columns.Add("Clinic Name");
            Globals.dtSearch.Columns.Add("Total Payment");
            Globals.dtSearch.Columns.Add("Note");

            foreach (Prescription p in Globals.PrescriptionList)
            {
                string drName = p.DrFirstName + " " + p.DrLastName;
                Globals.dtSearch.Rows.Add(p.PrescriptionID.ToString(), p.PatientID.ToString(), p.Prescriptiondate.ToString("dd/mm/yyyy"),
                    drName, p.DrID.ToString(), p.DrEmailAddress, p.DrPhone, p.DrExtension, p.DrClinicName, p.TotalPayment.ToString("0.00"),
                    p.note);
            }
            Globals.dtSearch.AcceptChanges();

            //lvSearch.ItemsSource = Globals.dtSearch.Rows;
            lvSearch.DataContext = Globals.dtSearch;

            gvSearch.Columns.Clear();

            foreach (var col in Globals.dtSearch.Columns)
            {
                DataColumn dc = (DataColumn)col;
                GridViewColumn column = new GridViewColumn();
                column.DisplayMemberBinding = new Binding(dc.ColumnName);
                column.Header = dc.ColumnName;
                gvSearch.Columns.Add(column);

                Binding bind = new Binding();
                lvSearch.SetBinding(ListView.ItemsSourceProperty, bind);
            }
        }
    }
}
