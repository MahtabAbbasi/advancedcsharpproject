﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void prescriptionsExcelSheet(int i)
        {
            Globals.collection[i].Cells[1, 1 ] = "Prescription Id";
            Globals.collection[i].Cells[1, 2 ] = "Patient Id";
            Globals.collection[i].Cells[1, 3 ] = "Prescription Date";
            Globals.collection[i].Cells[1, 4 ] = "Dr's First Name";
            Globals.collection[i].Cells[1, 5 ] = "Dr's Last Name";
            Globals.collection[i].Cells[1, 6 ] = "Dr's Id";
            Globals.collection[i].Cells[1, 7 ] = "Email";
            Globals.collection[i].Cells[1, 8 ] = "Phone No.";
            Globals.collection[i].Cells[1, 9 ] = "Extension";
            Globals.collection[i].Cells[1, 10] = "Clinic Name";
            Globals.collection[i].Cells[1, 11] = "Total Payment";
            Globals.collection[i].Cells[1, 12] = "Note";

            int j = 1;
            foreach (Prescription p in Globals.PrescriptionList)
            {
                j = j + 1;
                Globals.collection[i].Cells[j, 1] = p.PrescriptionID.ToString();
                Globals.collection[i].Cells[j, 2] = p.PatientID.ToString();
                Globals.collection[i].Cells[j, 3] = p.Prescriptiondate.ToString("dd/mm/yyyy");
                Globals.collection[i].Cells[j, 4] = p.DrFirstName;
                Globals.collection[i].Cells[j, 5] = p.DrLastName;
                Globals.collection[i].Cells[j, 6] = p.DrID.ToString();
                Globals.collection[i].Cells[j, 7] = p.DrEmailAddress;
                Globals.collection[i].Cells[j, 8] = p.DrPhone;
                Globals.collection[i].Cells[j, 9] = p.DrExtension;
                Globals.collection[i].Cells[j, 10] = p.DrClinicName;
                Globals.collection[i].Cells[j, 11] = p.TotalPayment.ToString("0.00");
                Globals.collection[i].Cells[j, 12] = p.note;
            }
        }
    }
}
