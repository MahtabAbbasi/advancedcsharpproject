﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void prescriptionsListView()
        {
            var prescriptions = (from me in Globals.ctx.PrescriptionCollection select me).ToList();
            Globals.PrescriptionList = new ObservableCollection<Prescription>(prescriptions);

            Globals.dtReport = new System.Data.DataTable();

            Globals.dtReport.Columns.Add("Prescription Id");
            Globals.dtReport.Columns.Add("Patient Id");
            Globals.dtReport.Columns.Add("Prescription Date");
            Globals.dtReport.Columns.Add("Dr's Name");
            Globals.dtReport.Columns.Add("Dr's Id");
            Globals.dtReport.Columns.Add("Email");
            Globals.dtReport.Columns.Add("Phone No.");
            Globals.dtReport.Columns.Add("Extension");
            Globals.dtReport.Columns.Add("Clinic Name");
            Globals.dtReport.Columns.Add("Total Payment");
            Globals.dtReport.Columns.Add("Note");

            foreach (Prescription p in Globals.PrescriptionList)
            {
                string drName = p.DrFirstName + " " + p.DrLastName;
                Globals.dtReport.Rows.Add(p.PrescriptionID.ToString(), p.PatientID.ToString(), p.Prescriptiondate.ToString("dd/mm/yyyy"),
                    drName, p.DrID.ToString(), p.DrEmailAddress, p.DrPhone, p.DrExtension, p.DrClinicName, p.TotalPayment.ToString("0.00"),
                    p.note);
            }
            Globals.dtReport.AcceptChanges();

            //lvReport.ItemsSource = Globals.dtReport.Rows;
            lvReport.DataContext = Globals.dtReport;

            gvReport.Columns.Clear();

            foreach (var col in Globals.dtReport.Columns)
            {
                DataColumn dc = (DataColumn)col;
                GridViewColumn column = new GridViewColumn();
                column.DisplayMemberBinding = new Binding(dc.ColumnName);
                column.Header = dc.ColumnName;
                gvReport.Columns.Add(column);

                Binding bind = new Binding();
                lvReport.SetBinding(ListView.ItemsSourceProperty, bind);
            }
        }
    }
}
