﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void refillsListView()
        {
            var refills = (from me in Globals.ctx.RefillCollection select me).ToList();
            Globals.RefillList = new ObservableCollection<Refill>(refills);

            Globals.dtReport = new System.Data.DataTable();

            Globals.dtReport.Columns.Add("Refill Id");
            Globals.dtReport.Columns.Add("Refill Date");
            Globals.dtReport.Columns.Add("Prescription Item Id");
            Globals.dtReport.Columns.Add("Note");

            foreach (Refill r in Globals.RefillList)
            {
                Globals.dtReport.Rows.Add(r.RefillID.ToString(), r.RefillDate.ToString("dd.MM.yyyy"), 
                    r.PrescriptionItemID.ToString(), r.note);
            }
            Globals.dtReport.AcceptChanges();

            //lvReport.ItemsSource = Globals.dtReport.Rows;
            lvReport.DataContext = Globals.dtReport;

            gvReport.Columns.Clear();

            foreach (var col in Globals.dtReport.Columns)
            {
                DataColumn dc = (DataColumn)col;
                GridViewColumn column = new GridViewColumn();
                column.DisplayMemberBinding = new Binding(dc.ColumnName);
                column.Header = dc.ColumnName;
                gvReport.Columns.Add(column);

                Binding bind = new Binding();
                lvReport.SetBinding(ListView.ItemsSourceProperty, bind);
            }
        }
    }
}
