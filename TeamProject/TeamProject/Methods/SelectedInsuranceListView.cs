﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void SelectedInsuranceListView()
        {
            Globals.dtSearch = new System.Data.DataTable();

            Globals.dtSearch.Columns.Add("Insurance Card Id");
            Globals.dtSearch.Columns.Add("Coverage Percent");
            Globals.dtSearch.Columns.Add("Insurance Company Name");
            Globals.dtSearch.Columns.Add("Insurance Type");

            foreach (Insurance iu in Globals.insuranceList)
            {
                Globals.dtSearch.Rows.Add(iu.InsuranceCardID.ToString(), iu.CoveragePercent.ToString("0.00"),
                    iu.InsuranceCompanyName, iu.InsuranceType);
            }
            Globals.dtSearch.AcceptChanges();

            //lvSearch.ItemsSource = Globals.dtSearch.Rows;
            lvSearch.DataContext = Globals.dtSearch;

            gvSearch.Columns.Clear();

            foreach (var col in Globals.dtSearch.Columns)
            {
                DataColumn dc = (DataColumn)col;
                GridViewColumn column = new GridViewColumn();
                column.DisplayMemberBinding = new Binding(dc.ColumnName);
                column.Header = dc.ColumnName;
                gvSearch.Columns.Add(column);

                Binding bind = new Binding();
                lvSearch.SetBinding(ListView.ItemsSourceProperty, bind);
            }
        }
    }
}
