﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void medicinesListView()
        {
            var medicines = (from me in Globals.ctx.MedicineCollection select me).ToList();
            Globals.medicinesList = new ObservableCollection<Medicine>(medicines);

            Globals.dtReport = new System.Data.DataTable();

            Globals.dtReport.Columns.Add("Medicine Id");
            Globals.dtReport.Columns.Add("Medicine Name");
            Globals.dtReport.Columns.Add("Dosage");
            Globals.dtReport.Columns.Add("Unit of Dosage");
            Globals.dtReport.Columns.Add("Unit");
            Globals.dtReport.Columns.Add("Unit Price");
            Globals.dtReport.Columns.Add("Note");

            foreach (Medicine m in Globals.medicinesList)
            {
                Globals.dtReport.Rows.Add(m.MedicineID.ToString(), m.MedicineName, m.Dosage.ToString(), 
                    m.DosageUnit, m.Unit.ToString(), m.UnitPrice.ToString(), m.Note);
            }
            Globals.dtReport.AcceptChanges();

            //lvReport.ItemsSource = Globals.dtReport.Rows;
            lvReport.DataContext = Globals.dtReport;

            gvReport.Columns.Clear();

            foreach (var col in Globals.dtReport.Columns)
            {
                DataColumn dc = (DataColumn)col;
                GridViewColumn column = new GridViewColumn();
                column.DisplayMemberBinding = new Binding(dc.ColumnName);
                column.Header = dc.ColumnName;
                gvReport.Columns.Add(column);

                Binding bind = new Binding();
                lvReport.SetBinding(ListView.ItemsSourceProperty, bind);
            }
        }
    }
}
