﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void SelectedPaymentsListView()
        {
            Globals.dtSearch = new System.Data.DataTable();

            Globals.dtSearch.Columns.Add("Payment Id");
            Globals.dtSearch.Columns.Add("Payment Amount");
            Globals.dtSearch.Columns.Add("Payment Date");
            Globals.dtSearch.Columns.Add("Payment Type");
            Globals.dtSearch.Columns.Add("Refill Id");

            foreach (Payment p in Globals.PaymentsList)
            {
                Globals.dtSearch.Rows.Add(p.PaymentID.ToString(), p.PaymentAmount.ToString("0.00"),
                    p.PaymentDate.ToString("dd.MM.yyyy"), p.PaymentType, p.RefillId.ToString());
            }
            Globals.dtSearch.AcceptChanges();

            //lvSearch.ItemsSource = Globals.dtSearch.Rows;
            lvSearch.DataContext = Globals.dtSearch;

            gvSearch.Columns.Clear();

            foreach (var col in Globals.dtSearch.Columns)
            {
                DataColumn dc = (DataColumn)col;
                GridViewColumn column = new GridViewColumn();
                column.DisplayMemberBinding = new Binding(dc.ColumnName);
                column.Header = dc.ColumnName;
                gvSearch.Columns.Add(column);

                Binding bind = new Binding();
                lvSearch.SetBinding(ListView.ItemsSourceProperty, bind);
            }
        }
    }
}
