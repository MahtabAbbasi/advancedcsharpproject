﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void patientsListView()
        {
            var patients = (from me in Globals.ctx.PatientCollection select me).ToList();
            Globals.patientsList = new ObservableCollection<Patient>(patients);
            //lvReport.ItemsSource = Globals.patientsList;

            Globals.dtReport = new System.Data.DataTable();

            Globals.dtReport.Columns.Add("Patient Id");
            Globals.dtReport.Columns.Add("Name");
            Globals.dtReport.Columns.Add("Gender");
            Globals.dtReport.Columns.Add("Insurance Card Id");
            Globals.dtReport.Columns.Add("Date of Birth");
            Globals.dtReport.Columns.Add("Adress");
            Globals.dtReport.Columns.Add("Home Phone No.");
            Globals.dtReport.Columns.Add("Cell Phone No.");
            Globals.dtReport.Columns.Add("Dr's Name");
            Globals.dtReport.Columns.Add("Dr's Id");
            Globals.dtReport.Columns.Add("Email");
            Globals.dtReport.Columns.Add("Phone No.");
            Globals.dtReport.Columns.Add("Extension");
            Globals.dtReport.Columns.Add("Clinic Name");
            Globals.dtReport.Columns.Add("Note");

            foreach (Patient p in Globals.patientsList)
            {
                string name = p.FirstName + " " + p.LastName;
                string address = p.Address + ", " + p.City + ", " + p.Region + " " + p.PostalCode;
                string drName = p.FamilyDrFirstName + " " + p.FamilyDrLastName;
                Globals.dtReport.Rows.Add(p.PatientID.ToString(), name, p.Gender.ToString(), p.InsuranceCardID.ToString(),
                                       p.BirthDate.ToString("dd/mm/yyyy"), address, p.HomePhone, p.CellPhone, drName,
                                       p.FamilyDrID.ToString(), p.FamilyDrEmailAddress, p.FamilyDrPhone, 
                                       p.FamilyDrExtension, p.FamilyDrClinicName, p.note);
            }
            Globals.dtReport.AcceptChanges();

            //lvReport.ItemsSource = Globals.dtReport.Rows;
            lvReport.DataContext = Globals.dtReport;

            gvReport.Columns.Clear();

            foreach (var col in Globals.dtReport.Columns)
            {
                DataColumn dc = (DataColumn)col;
                GridViewColumn column = new GridViewColumn();
                column.DisplayMemberBinding = new Binding(dc.ColumnName);
                column.Header = dc.ColumnName;
                gvReport.Columns.Add(column);

                Binding bind = new Binding();
                lvReport.SetBinding(ListView.ItemsSourceProperty, bind);
            }
        }
    }
}
