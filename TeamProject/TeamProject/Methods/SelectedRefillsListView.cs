﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void SelectedRefillsListView()
        {
            Globals.dtSearch = new System.Data.DataTable();

            Globals.dtSearch.Columns.Add("Refill Id");
            Globals.dtSearch.Columns.Add("Refill Date");
            Globals.dtSearch.Columns.Add("Prescription Item Id");
            Globals.dtSearch.Columns.Add("Note");

            foreach (Refill r in Globals.RefillList)
            {
                Globals.dtSearch.Rows.Add(r.RefillID.ToString(), r.RefillDate.ToString("dd.MM.yyyy"), 
                    r.PrescriptionItemID.ToString(), r.note);
            }
            Globals.dtSearch.AcceptChanges();

            //lvSearch.ItemsSource = Globals.dtSearch.Rows;
            lvSearch.DataContext = Globals.dtSearch;

            gvSearch.Columns.Clear();

            foreach (var col in Globals.dtSearch.Columns)
            {
                DataColumn dc = (DataColumn)col;
                GridViewColumn column = new GridViewColumn();
                column.DisplayMemberBinding = new Binding(dc.ColumnName);
                column.Header = dc.ColumnName;
                gvSearch.Columns.Add(column);

                Binding bind = new Binding();
                lvSearch.SetBinding(ListView.ItemsSourceProperty, bind);
            }
        }
    }
}
