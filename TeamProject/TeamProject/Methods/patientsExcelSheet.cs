﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Windows;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void patientsExcelSheet(int i)
        {
            Globals.collection[i].Cells[1, 1 ] = "Patient Id";
            Globals.collection[i].Cells[1, 2 ] = "First Name";
            Globals.collection[i].Cells[1, 3 ] = "Last Name";
            Globals.collection[i].Cells[1, 4 ] = "Gender";
            Globals.collection[i].Cells[1, 5 ] = "Insurance Card Id";
            Globals.collection[i].Cells[1, 6 ] = "Date of Birth";
            Globals.collection[i].Cells[1, 7 ] = "Adress";
            Globals.collection[i].Cells[1, 8 ] = "City";
            Globals.collection[i].Cells[1, 9 ] = "Region";
            Globals.collection[i].Cells[1, 10] = "Postal Code";
            Globals.collection[i].Cells[1, 11] = "Country";
            Globals.collection[i].Cells[1, 12] = "Home Phone No.";
            Globals.collection[i].Cells[1, 13] = "Cell Phone No.";
            Globals.collection[i].Cells[1, 14] = "Dr's First Name";
            Globals.collection[i].Cells[1, 15] = "Dr's Last Name";
            Globals.collection[i].Cells[1, 16] = "Dr's Id";
            Globals.collection[i].Cells[1, 17] = "Email";
            Globals.collection[i].Cells[1, 18] = "Phone No.";
            Globals.collection[i].Cells[1, 19] = "Extension";
            Globals.collection[i].Cells[1, 20] = "Clinic Name";
            Globals.collection[i].Cells[1, 21] = "Note";

            int j = 1;
            foreach (Patient p in Globals.patientsList)
            {
                j = j + 1;
                Globals.collection[i].Cells[j, 1 ] = p.PatientID;
                Globals.collection[i].Cells[j, 2 ] = p.FirstName;
                Globals.collection[i].Cells[j, 3 ] = p.LastName;
                Globals.collection[i].Cells[j, 4 ] = p.Gender.ToString();
                Globals.collection[i].Cells[j, 5 ] = p.InsuranceCardID.ToString();
                Globals.collection[i].Cells[j, 6 ] = p.BirthDate.ToString("dd/mm/yyyy");
                Globals.collection[i].Cells[j, 7 ] = p.Address;
                Globals.collection[i].Cells[j, 8 ] = p.City;
                Globals.collection[i].Cells[j, 9 ] = p.Region;
                Globals.collection[i].Cells[j, 10] = p.PostalCode;
                Globals.collection[i].Cells[j, 11] = p.Country;
                Globals.collection[i].Cells[j, 12] = p.HomePhone;
                Globals.collection[i].Cells[j, 13] = p.CellPhone;
                Globals.collection[i].Cells[j, 14] = p.FamilyDrFirstName;
                Globals.collection[i].Cells[j, 15] = p.FamilyDrLastName;
                Globals.collection[i].Cells[j, 16] = p.FamilyDrID.ToString();
                Globals.collection[i].Cells[j, 17] = p.FamilyDrEmailAddress;
                Globals.collection[i].Cells[j, 18] = p.FamilyDrPhone;
                Globals.collection[i].Cells[j, 19] = p.FamilyDrExtension;
                Globals.collection[i].Cells[j, 20] = p.FamilyDrClinicName;
                Globals.collection[i].Cells[j, 21] = p.note;
            }
        }
    }
}
