﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void paymentsListView()
        {
            var Payments = (from me in Globals.ctx.PaymentCollection select me).ToList();
            Globals.PaymentsList = new ObservableCollection<Payment>(Payments);

            Globals.dtReport = new System.Data.DataTable();

            Globals.dtReport.Columns.Add("Payment Id");
            Globals.dtReport.Columns.Add("Payment Amount");
            Globals.dtReport.Columns.Add("Payment Date");
            Globals.dtReport.Columns.Add("Payment Type");
            Globals.dtReport.Columns.Add("Refill Id");

            foreach (Payment p in Globals.PaymentsList)
            {
                Globals.dtReport.Rows.Add(p.PaymentID.ToString(), p.PaymentAmount.ToString("0.00"),
                    p.PaymentDate.ToString("dd.MM.yyyy"), p.PaymentType, p.RefillId.ToString());
            }
            Globals.dtReport.AcceptChanges();

            //lvReport.ItemsSource = Globals.dtReport.Rows;
            lvReport.DataContext = Globals.dtReport;

            gvReport.Columns.Clear();

            foreach (var col in Globals.dtReport.Columns)
            {
                DataColumn dc = (DataColumn)col;
                GridViewColumn column = new GridViewColumn();
                column.DisplayMemberBinding = new Binding(dc.ColumnName);
                column.Header = dc.ColumnName;
                gvReport.Columns.Add(column);

                Binding bind = new Binding();
                lvReport.SetBinding(ListView.ItemsSourceProperty, bind);
            }
        }
    }
}
