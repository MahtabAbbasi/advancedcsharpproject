﻿using System;
using System.Collections.ObjectModel;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TeamProject.Classes;

namespace TeamProject
{
    public partial class MainWindow
    {
        private void activeLists()
        {
            var patients = (from me in Globals.ctx.PatientCollection select me).ToList();
            Globals.patientsList = new ObservableCollection<Patient>(patients);

            var prescriptions = (from me in Globals.ctx.PrescriptionCollection select me).ToList();
            Globals.PrescriptionList = new ObservableCollection<Prescription>(prescriptions);

            var prescriptionItems = (from me in Globals.ctx.PrescriptionItemCollection select me).ToList();
            Globals.PrescriptionItemList = new ObservableCollection<PrescriptionItem>(prescriptionItems);

            var refills = (from me in Globals.ctx.RefillCollection select me).ToList();
            Globals.RefillList = new ObservableCollection<Refill>(refills);

            var medicines = (from me in Globals.ctx.MedicineCollection select me).ToList();
            Globals.medicinesList = new ObservableCollection<Medicine>(medicines);

            var Payments = (from me in Globals.ctx.PaymentCollection select me).ToList();
            Globals.PaymentsList = new ObservableCollection<Payment>(Payments);

            var insurance = (from me in Globals.ctx.InsuranceCollection select me).ToList();
            Globals.insuranceList = new ObservableCollection<Insurance>(insurance);
        }
    }
}
