﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TeamProject
{
    public class PharmacyContext : DbContext
    {
        public PharmacyContext() : base("name=DbConnectionTeamProject")
        {
        }

        public DbSet<Insurance> InsuranceCollection { get; set; }
        public DbSet<Medicine> MedicineCollection { get; set; }
        public DbSet<Patient> PatientCollection { get; set; }
        public DbSet<Payment> PaymentCollection { get; set; }
        public DbSet<Prescription> PrescriptionCollection { get; set; }
        public DbSet<PrescriptionItem> PrescriptionItemCollection { get; set; }
        public DbSet<Refill> RefillCollection { get; set; }
    }
}
