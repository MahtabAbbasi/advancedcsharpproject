/*Create Restaurant database
Sccript Date :September 10,2019
Developed by :  Mahtab Abbasigaravand/ Aziz Azimi
*/

create database PharmacyProject2018

on primary
(

	name = 'PharmacyProject2018',
	filename = 'E:\harmacyProject database\PharmacyProject2018.mdf',
	size = 12 MB,
	filegrowth = 2 MB,
	maxsize = 100 MB
) 
log on
(
	name = 'PharmacyProject2018_log',
	filename = 'E:\harmacyProject database\PharmacyProject2018_log.ldf',
	size = 3 MB,
	filegrowth = 10%,
	maxsize = 25 MB
)
;
go


