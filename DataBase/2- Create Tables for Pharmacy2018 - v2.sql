/*Create Restaurant database
Sccript Date :September 10,2019
Developed by : Mahtab Abbasigaravand/ Aziz Azimi
*/

use PharmacyProject2018
;
go


--create table Patients
if OBJECT_ID('Patients','U') is not null
drop table Patients
;
go
create table Patients
(
	PatientID int identity(1,1)not null ,
	LastName nvarchar(20) not null ,
	FirstName nvarchar(20) not null ,
	Title nvarchar(30) null ,
	FamilyDrLastName nvarchar(20) not null ,
	FamilyDrFirstName nvarchar(20) not null ,
	FamilyDrID nvarchar(20) not null ,
	FamilyDrEmailAddress nvarchar(30) not null ,
	FamilyDrPhone nvarchar(24) null ,
	FamilyDrExtension nvarchar(4) null,
	FamilyDrClinicName nvarchar(4) null,
	InsuranceCardID int not null,
	Allergies nvarchar(250) null,
	BirthDate date null,
	Address nvarchar(60) null,
	City nvarchar(15) null ,
	Region nvarchar(15) null,
	PostalCode nvarchar(10) null ,
	Country nvarchar(15) null ,
	CellPhone nvarchar(24) null ,
	HomePhone nvarchar(24) null ,
	Note nvarchar(250) null,
	constraint pk_Patients primary key (PatientID asc)
)
;
go



--create table Prescription
if OBJECT_ID('Prescriptions','U') is not null
drop table Prescriptions
;
go

create table Prescriptions

(
	PrescriptionID int identity(1,1) not null ,
	Date date not null ,
	PatientID int not null,
	DrLastName nvarchar(20) not null ,
	DrFirstName nvarchar(20) not null ,
	DrID nvarchar(20) not null ,
	DrEmailAddress nvarchar(30) not null ,
	DrPhone nvarchar(24) null ,
	DrExtension nvarchar(4) null,
	DrClinicName nvarchar(4) null,
	TotalPayment money not null,
	Note nvarchar(250) null,
	constraint pk_Prescriptions primary key (PrescriptionID asc)
)
;
go

--create table PrescriptionItems
if OBJECT_ID('PrescriptionItems','U') is not null
drop table PrescriptionItems
;
go
create table PrescriptionItems

(
	PrescriptionID int identity(1,1) not null ,
	MedicineID int not null,
	Quantity int not null,
	RefillID int not null ,
	NumberOfRefills int not null,
	RefillTimes int null,
	Instructions nvarchar(250) null,
	constraint pk_PrescriptionItems primary key (PrescriptionID asc, MedicineID asc)
)
;
go

--create table Refills
if OBJECT_ID('Refills','U') is not null
	drop table Refills
;
go

create table Refills
(
	RefillID int identity(1,1) not null ,
	RefillDate date not null,
	PaymentID int not null,
	Payment money not null,
	note nvarchar(250)
	constraint pk_Refills primary key clustered (RefillID asc)
)
;
go

--create table Medicine
if OBJECT_ID('Medicines','U') is not null
drop table Medicines
;
go

create table Medicines
(
	MedicineID int not null ,
	MedicineName nvarchar(50) not null ,
	Dosage int not null,
	Unit nvarchar(20) not null,
	UnitPrice money not null,
	Note nvarchar(150) not null ,
	constraint pk_Medicines primary key (MedicineID asc)
)
;
go



--create table Payments
if OBJECT_ID('Payments','U') is not null
drop table Payments
;
go

create table Payments
(
	PaymentID int identity(1,1) not null,
	PrescriptionID int not null,
	PaymentDate date not null,
	PaymentType nvarchar(20) not null,
	GST money not null,
	PST money not null,
	constraint pk_Payments primary key clustered (PaymentID asc)
)
;
go


--create table Insurance
if OBJECT_ID('Insurance','U') is not null
drop table Insurance
;
go

create table Insurance
(
	InsuranceCardID int not null ,--fk,pk
	CoveragePercent int  not null ,--fk,pk
	InsuranceCompanyName  datetime not null,
	InsuranceType nvarchar(50) not null,
	constraint pk_Insurance primary key clustered (InsuranceID asc) 
)
;
go



