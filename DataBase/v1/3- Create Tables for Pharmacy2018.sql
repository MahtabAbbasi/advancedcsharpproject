/*Create Restaurant database
Sccript Date :September 10,2019
Developed by : Mahtab Abbasigaravand/ Aziz Azimi
*/

use PharmacyProject2018
;
go


--create table Patients
if OBJECT_ID('Patients','U') is not null
drop table Patients
;
go
create table Patients
(
	PatientID int identity(1,1)not null ,
	LastName nvarchar(20) not null ,
	FirstName nvarchar(20) not null ,
	Title nvarchar(30) null ,
	FamilyDrLastName nvarchar(20) not null ,
	FamilyDrFirstName nvarchar(20) not null ,
	FamilyDrID nvarchar(20) not null ,
	FamilyDrEmailAddress nvarchar(30) not null ,
	FamilyDrPhone nvarchar(24) null ,
	FamilyDrExtension nvarchar(4) null,
	FamilyDrClinicName nvarchar(4) null,
	InsuranceID int not null,
	Allergies nvarchar(250) null,
	BirthDate datetime null,
	Address nvarchar(60) null,
	City nvarchar(15) null ,
	Region nvarchar(15) null,
	PostalCode nvarchar(10) null ,
	Country nvarchar(15) null ,
	CellPhone nvarchar(24) null ,
	HomePhone nvarchar(24) null ,
	Note nvarchar(250) null,
	constraint pk_Patients primary key (PatientID asc)
)
;
go

--create table Staff
if OBJECT_ID('Staff','U') is not null
drop table Staff
;
go

create table Staff
(
    EmployeeID int identity(1,1) not null ,
	LastName nvarchar(20) not null ,
	FirstName nvarchar(20) not null ,
	Title nvarchar(30) null ,
	Address nvarchar(60) null,
	City nvarchar(15) null ,
	Region nvarchar(15) null,
	JobPositionID int not null,
	PostalCode nvarchar(10) null ,
	Country nvarchar(15) null ,
	Phone nvarchar(24) null ,
	Photo nvarchar(200) null,
	Note nvarchar(250) null,
	constraint pk_Staff primary key (EmployeeID asc)
)
;
go


--create table JobPositions
if OBJECT_ID('JobPositions','U') is not null
drop table JobPositions
;
go
create table JobPositions
(
	JobPositionID int identity(1,1) not null ,
	Name nvarchar(50) not null ,
	Description int not null, 
	constraint pk_JobPositions primary key (JobPositionID asc)
)
;
go

--create table Suppliers
if OBJECT_ID('Suppliers','U') is not null
drop table Suppliers
;
go

create table Suppliers
(
	SupplierID int identity(1,1) not null ,
	CompanyName nvarchar(40) null,
	LastName nvarchar(20) not null ,
	FirstName nvarchar(10) not null ,
	Address nvarchar(60) null,
	City nvarchar(15) null ,
	Region nvarchar(15) null,
	PostalCode nvarchar(10) null ,
	Country nvarchar(15) null ,
	Phone nvarchar(24) null ,
	Extension nvarchar(4) null,
	Note nvarchar(250) null,
	constraint pk_Suppliers primary key (SupplierID asc)
)
;
go


--create table Logins
if OBJECT_ID('Logins','U') is not null
drop table Logins
;
go
create table Logins
(
	UserName int identity(1,1) not null ,
	EmployeeID int not null ,
	Password nvarchar(50) not null ,
	SecurityLevelID int not null, 
	constraint pk_Logins primary key (UserName asc)
)
;
go


--create table SecurityLevels
if OBJECT_ID('SecurityLevels','U') is not null
drop table SecurityLevels
;
go
create table SecurityLevels
(
	SecurityLevelID int identity(1,1) not null ,
	Name nvarchar(50) not null ,
	Description int not null, 
	constraint pk_SecurityLevels primary key (SecurityLevelID asc)
)
;
go




--create table Prescription
if OBJECT_ID('Prescriptions','U') is not null
drop table Prescriptions
;
go

create table Prescriptions

(
	PrescriptionID int identity(1,1) not null ,
	Date nvarchar(20) not null ,
	PatientID int not null,
	EmployeeID int not null,
	TotalPayment int not null,
	Note nvarchar(250) null,
	constraint pk_Prescriptions primary key (PrescriptionID asc)
)
;
go

--create table PrescriptionItems
if OBJECT_ID('PrescriptionItems','U') is not null
drop table PrescriptionItems
;
go
create table PrescriptionItems

(
	PrescriptionID int identity(1,1) not null ,
	MedicineID int not null,
	Quantity int not null,
	RefillID int not null ,
	NumberOfRefills int not null,
	RefillPeriod int null,
	Dosage int not null,
	QuantityPerDay int not null,
	Instructions nvarchar(250) null,
	constraint pk_PrescriptionItems primary key (PrescriptionID asc, MedicineID asc)
)
;
go

--create table Refills
if OBJECT_ID('Refills','U') is not null
	drop table Refills
;
go

create table Refills
(
	RefillID int identity(1,1) not null ,
	RefillDate datetime not null,
	PaymentID int not null,
	note nvarchar(250)
	constraint pk_Refills primary key clustered (PrescriptionID asc,MedicineID asc,RefillDate asc)
)
;
go

--create table Medicine
if OBJECT_ID('Medicines','U') is not null
drop table Medicines
;
go

create table Medicines
(
	MedicineID int not null ,
	MedicineName nvarchar(50) not null ,
	Unit nvarchar(20) not null,
	UnitPrice money not null,
	Note nvarchar(150) not null ,
	constraint pk_Medicines primary key (MedicineID asc)
)
;
go



--create table Payments
if OBJECT_ID('Payments','U') is not null
drop table Payments
;
go

create table Payments
(
	PaymentID int identity(1,1) not null,
	InsuranceID int not null,
	PaymentDate date not null,
	PaymentType nvarchar(20) not null,
	constraint pk_Payments primary key clustered (PaymentID asc)
)
;
go


--create table Inventory
if OBJECT_ID('Inventory','U') is not null
	drop table Inventory
;
go

create table Inventory
(
	MedicineID int not null ,--fk,pk
	SupplierID int  not null ,--fk,pk
	Quantity int not null,--decimal
	Unit  nvarchar(10) not null,
	UnitPrice  money not null,
	constraint pk_Inventory primary key clustered (MedicineID asc,SupplierID asc)
)
;
go

--create table SupplyDetails
if OBJECT_ID('SupplyDetails','U') is not null
drop table SupplyDetails
;
go

create table SupplyDetails
(
	MedicineID int not null ,--fk,pk
	SupplierID int  not null ,--fk,pk
	PerchaseDate  datetime not null,
	Quantity int not null,--decimal
	Unit  nvarchar(10) not null,
	UnitPrice  money not null
	constraint pk_SupplyDetails primary key clustered (MedicineID asc,SupplierID asc,PerchaseDate asc) 
)
;
go


--create table Insurance
if OBJECT_ID('Insurance','U') is not null
drop table Insurance
;
go

create table Insurance
(
	InsuranceID int not null ,--fk,pk
	AmountPaied int  not null ,--fk,pk
	InsuranceName  datetime not null,
	constraint pk_Insurance primary key clustered (InsuranceID asc) 
)
;
go



