/*Create Restaurant database
Sccript Date :September 10,2019
Developed by : Mahtab Abbasigaravand/ Aziz Azimi
*/

use PharmacyProject2018
;
go

/****************************************Foreign key constraints************************************************/


/* 1)  */
	alter table Patients
	add constraint fk_Patients_Insurance foreign key (InsuranceID) references Insurance (InsuranceID)
	;
	go
	


/* 5)  */
	alter table Prescriptions
	add constraint fk_Prescriptions_Patients foreign key (PatientID) references Patients (PatientID)
	;
	go


	
	alter table PrescriptionItems
	add constraint fk_PrescriptionItems_Prescriptions foreign key (PrescriptionID) references Prescriptions (PrescriptionID)
	;
	go
	
/* 9)  */
	alter table PrescriptionItems
	add constraint fk_PrescriptionItems_Medicines foreign key (MedicineID) references Medicines(MedicineID)
	;


	
	alter table PrescriptionItems
	add constraint fk_PrescriptionItems_Refills foreign key (RefillID) references Refills(RefillID)
	;

	
/* 8)  */
	alter table Payments
	add constraint fk_Payments_Refills foreign key (RefillID) references Refills(RefillID)
	;

	/* 8)  */
	alter table Payments
	add constraint fk_Payments_Insurances foreign key (InsuranceID) references Insurances(InsuranceID)
	;


























